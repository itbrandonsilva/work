#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <string>
#include <cmath>
#include <RVO.h>

//Screen dimension constants
const int SCREEN_WIDTH = 1280;
const int SCREEN_HEIGHT = 720;

//Starts up SDL and creates window
bool init();

//Loads media
bool loadMedia();

//Frees media and shuts down SDL
void close();

//Loads individual image as texture
SDL_Texture* loadTexture( std::string path );

//The window we'll be rendering to
SDL_Window* gWindow = NULL;

//The window renderer
SDL_Renderer* gRenderer = NULL;

bool init()
{
    //Initialization flag
    bool success = true;

    //Initialize SDL
    if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
    {
        printf( "SDL could not initialize! SDL Error: %s\n", SDL_GetError() );
        success = false;
    }
    else
    {
        //Set texture filtering to linear
        if( !SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" ) )
        {
            printf( "Warning: Linear texture filtering not enabled!" );
        }

        //Create window
        gWindow = SDL_CreateWindow( "SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN );
        if( gWindow == NULL )
        {
            printf( "Window could not be created! SDL Error: %s\n", SDL_GetError() );
            success = false;
        }
        else
        {
            //Create renderer for window
            gRenderer = SDL_CreateRenderer( gWindow, -1, SDL_RENDERER_ACCELERATED );
            if( gRenderer == NULL )
            {
                printf( "Renderer could not be created! SDL Error: %s\n", SDL_GetError() );
                success = false;
            }
            else
            {
                //Initialize renderer color
                SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );

                //Initialize PNG loading
                int imgFlags = IMG_INIT_PNG;
                if( !( IMG_Init( imgFlags ) & imgFlags ) )
                {
                    printf( "SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError() );
                    success = false;
                }
            }
        }
    }

    return success;
}

bool loadMedia()
{
    //Loading success flag
    bool success = true;

    //Nothing to load
    return success;
}

void close()
{
    //Destroy window    
    SDL_DestroyRenderer( gRenderer );
    SDL_DestroyWindow( gWindow );
    gWindow = NULL;
    gRenderer = NULL;

    //Quit SDL subsystems
    IMG_Quit();
    SDL_Quit();
}

SDL_Texture* loadTexture( std::string path )
{
    //The final texture
    SDL_Texture* newTexture = NULL;

    //Load image at specified path
    SDL_Surface* loadedSurface = IMG_Load( path.c_str() );
    if( loadedSurface == NULL )
    {
        printf( "Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError() );
    }
    else
    {
        //Create texture from surface pixels
        newTexture = SDL_CreateTextureFromSurface( gRenderer, loadedSurface );
        if( newTexture == NULL )
        {
            printf( "Unable to create texture from %s! SDL Error: %s\n", path.c_str(), SDL_GetError() );
        }

        //Get rid of old loaded surface
        SDL_FreeSurface( loadedSurface );
    }

    return newTexture;
}

void draw_circle(SDL_Renderer* renderer, SDL_Point center, int radius, SDL_Color color)
{
    SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a);
    for (int w = 0; w < radius * 2; w++)
    {
        for (int h = 0; h < radius * 2; h++)
        {
            int dx = radius - w; // horizontal offset
            int dy = radius - h; // vertical offset
            if ((dx*dx + dy*dy) <= (radius * radius))
            {
                SDL_RenderDrawPoint(renderer, center.x + dx, center.y + dy);
            }
        }
    }
}


            std::vector<RVO::Vector2> goals;

void setupScenario(RVO::RVOSimulator* sim) {
  // Specify global time step of the simulation.
  sim->setTimeStep(0.25f);

  // Specify default parameters for agents that are subsequently added.
  //sim->setAgentDefaults(5.0f, 10, 10.0f, 5.0f, 2.0f, 0.2f);
  sim->setAgentDefaults(15.0f, 10, 10.0f, 5.0f, 8.0f, 16.0f);

  // Add agents, specifying their start position.
  sim->addAgent(RVO::Vector2(-50.0f, -50.0f));
  sim->addAgent(RVO::Vector2(-40.0f, -50.0f));
  sim->addAgent(RVO::Vector2(-30.0f, -50.0f));
  sim->addAgent(RVO::Vector2(-20.0f, -50.0f));
  sim->addAgent(RVO::Vector2(-10.0f, -50.0f));
  sim->addAgent(RVO::Vector2(0     , -40.0f));
  sim->addAgent(RVO::Vector2(-50.0f, -40.0f));
  sim->addAgent(RVO::Vector2(-40.0f, -40.0f));
  sim->addAgent(RVO::Vector2(-30.0f, -40.0f));
  sim->addAgent(RVO::Vector2(-20.0f, -40.0f));
  sim->addAgent(RVO::Vector2(-10.0f, -40.0f));
  sim->addAgent(RVO::Vector2(0     , -30.0f));
  sim->addAgent(RVO::Vector2(-50.0f, -30.0f));
  sim->addAgent(RVO::Vector2(-40.0f, -30.0f));
  sim->addAgent(RVO::Vector2(-30.0f, -30.0f));
  sim->addAgent(RVO::Vector2(-20.0f, -30.0f));
  sim->addAgent(RVO::Vector2(-10.0f, -30.0f));
  sim->addAgent(RVO::Vector2(0     , -30.0f));



  // Create goals (simulator is unaware of these).
  for (size_t i = 0; i < sim->getNumAgents(); ++i) {
    //goals.push_back(-sim->getAgentPosition(i));
    goals.push_back(RVO::Vector2(700, 700));
  }

  // Add (polygonal) obstacle(s), specifying vertices in counterclockwise order.
  std::vector<RVO::Vector2> vertices;
  vertices.push_back(RVO::Vector2(-7.0f, -20.0f));
  vertices.push_back(RVO::Vector2(7.0f, -20.0f));
  vertices.push_back(RVO::Vector2(7.0f, 20.0f));
  vertices.push_back(RVO::Vector2(-7.0f, 20.0f));

  //sim->addObstacle(vertices);

  // Process obstacles so that they are accounted for in the simulation.
  sim->processObstacles();
}


void setPreferredVelocities(RVO::RVOSimulator* sim) {
  // Set the preferred velocity for each agent.
  for (size_t i = 0; i < sim->getNumAgents(); ++i) {
    if (absSq(goals[i] - sim->getAgentPosition(i)) < sim->getAgentRadius(i) * sim->getAgentRadius(i) ) {
      // Agent is within one radius of its goal, set preferred velocity to zero
      sim->setAgentPrefVelocity(i, RVO::Vector2(0.0f, 0.0f));
    } else {
      // Agent is far away from its goal, set preferred velocity as unit vector towards agent's goal.
      sim->setAgentPrefVelocity(i, normalize(goals[i] - sim->getAgentPosition(i)) * 4);
    }
  }
}

int main( int argc, char* args[] )
{
    //Start up SDL and create window
    if( !init() )
    {
        printf( "Failed to initialize!\n" );
    }
    else
    {
        //Load media
        if( !loadMedia() )
        {
            printf( "Failed to load media!\n" );
        }
        else
        {   
            //Main loop flag
            bool quit = false;

            //Event handler
            SDL_Event e;

            //RVO::RVOSimulator* sim = RVO::RVOSimulator();
            RVO::RVOSimulator* sim = NULL;
            sim = new RVO::RVOSimulator();

            setupScenario(sim);

            SDL_Color red = {0xFF, 0x00, 0x00, 0xFF};

            //While application is running
            while( !quit )
            {
                //Handle events on queue
                while( SDL_PollEvent( &e ) != 0 )
                {
                    //User requests quit
                    if( e.type == SDL_QUIT )
                    {
                        quit = true;
                    }
                }

                setPreferredVelocities(sim);
                sim->doStep();

                //Clear screen
                SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );
                SDL_RenderClear( gRenderer );

  for (size_t i = 0; i < sim->getNumAgents(); ++i) {
    const RVO::Vector2 pos = sim->getAgentPosition(i);
SDL_Point loc = {pos.x(), pos.y()};
                //Render red filled quad
                //SDL_Rect fillRect = { SCREEN_WIDTH / 4, SCREEN_HEIGHT / 4, SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2 };

                /*
                SDL_Rect fillRect = { pos.x(), pos.y(), 16, 16 };
                SDL_SetRenderDrawColor( gRenderer, 0xFF, 0x00, 0x00, 0xFF );        
                SDL_RenderFillRect( gRenderer, &fillRect );
                */

                draw_circle( gRenderer, loc, 8, red);

                /*
                //Render green outlined quad
                SDL_Rect outlineRect = { SCREEN_WIDTH / 6, SCREEN_HEIGHT / 6, SCREEN_WIDTH * 2 / 3, SCREEN_HEIGHT * 2 / 3 };
                SDL_SetRenderDrawColor( gRenderer, 0x00, 0xFF, 0x00, 0xFF );        
                SDL_RenderDrawRect( gRenderer, &outlineRect );
                
                //Draw blue horizontal line
                SDL_SetRenderDrawColor( gRenderer, 0x00, 0x00, 0xFF, 0xFF );        
                SDL_RenderDrawLine( gRenderer, 0, SCREEN_HEIGHT / 2, SCREEN_WIDTH, SCREEN_HEIGHT / 2 );

                //Draw vertical line of yellow dots
                SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0x00, 0xFF );
                for( int i = 0; i < SCREEN_HEIGHT; i += 4 )
                {
                    SDL_RenderDrawPoint( gRenderer, SCREEN_WIDTH / 2, i );
                }
                */

                //Update screen
            }
                SDL_RenderPresent( gRenderer );

  }

            delete sim;
        }
    }

    //Free resources and close SDL
    close();

    return 0;
}
