// Removed insecure
// Removed autopublish
// Added accounts-base

var Users = new Mongo.Collection('users');

if (Meteor.isServer) {
  Meteor.methods({
    'login': function (userId, cb) {
      this.setUserId(userId.toString());
      console.log("LOGGED IN:", userId);
      return userId;
    },
    'logout': function (cb) {
      this.setUserId(null);
      console.log("LOGGED OUT");
    }
  });

  Meteor.publish(null, function () {
    return Users.find();
  });
}

if (Meteor.isClient) {
  // counter starts at 0
  Session.setDefault('counter', 0);

  Template.hello.helpers({
    // This is what the template uses when rendering 'counter'
    counter: function () {
      return Session.get('counter');
    }
  });

  Template.hello.events({
    'click .click': function () {
      // increment the counter when button is clicked
      Session.set('counter', Session.get('counter') + 1);
    },
    'click .login': function () {
      Meteor.call('login', Session.get('counter'), function (err, userId) {
        if (err) throw err;
        Meteor.connection.setUserId(userId.toString());
        Router.go('/');
      });
    }
  });

  Template.home.events({
    'click .logout': function () {
      Meteor.call('logout', function (err) {
        if (err) throw err;
        Meteor.connection.setUserId(null);
        Router.go('/login');
      });
    }
  });
}

if (Meteor.isServer) {
  Meteor.startup(function () {
    // code to run on server at startup
  });
}

//setInterval(function () {
//    console.log(Meteor.userId);
//}, 1000);

//Router.Location.configure({useHashPaths: true});
//console.log(Object.keys(Iron));

Router.onBeforeAction(function () {
  console.log('onBeforeAction()');
  var userId = Meteor.connection.userId();
  console.log('USERID:', userId);
  if ( this.url != '/login' && ! userId ) {
    console.log('REDIRECT');
    this.redirect('/login');
  }
  this.next();
}, {except: ['login']});

Router.route('/', function () {
  console.log("RENDER HOME");
  this.render('home');
});

Router.route('/login', function () {
  console.log("RENDER LOGIN");
  this.render('login');
});
