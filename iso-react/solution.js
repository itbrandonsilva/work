var React = require('react');
var ReactDOMServer = require('react-dom/server');
var DOM = React.DOM;
var body = DOM.body;
var div = DOM.div;
var script = DOM.script;

var fs = require('fs');

var browserify = require('browserify');
var babelify = require("babelify");
var presets = ['react', 'es2015'];

var express = require('express');
var app = express();

// http://stackoverflow.com/questions/33704714/cant-require-default-export-value-in-babel-6-x
require('babel-register')({presets});
var TodoBox = require('./views/index.jsx').default;

var data = [
    {title: 'Shopping', detail: "Need to buy some things."},
    {title: 'Hair cut', detail: "My hair is a mess."}
];

app.use('/bundle.js', function (req, res) {
    res.setHeader('content-type', 'application/javascript');
    browserify("app.js")
        .transform(babelify.configure({presets}))
        .bundle()
        .pipe(res);
});

app.use('/', function (req, res) {
    var initialData = JSON.stringify(data);

    data = [
        {title: 'Shopping', detail: "Need to buy some things.111"},
        {title: 'Hair cut', detail: "My hair is a mess.111"}
    ];

    var markup = ReactDOMServer.renderToString(React.createElement(TodoBox, {data: data}));

    res.setHeader('Content-Type', 'text/html');

    var html = ReactDOMServer.renderToStaticMarkup(body(null,
        div({id: 'app', dangerouslySetInnerHTML: {__html: markup}}),
        script({
            id: 'initial-data',
            type: 'text/plain',
            'data-json': initialData
        }),
        script({src: '/bundle.js'})
    ));

    res.end(html);
});

app.listen(3000, function () { 
    console.log("Listening...");
});
