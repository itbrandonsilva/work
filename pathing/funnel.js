function initPortals(path, startPos, endPos) {
    startPos = startPos || path[0].center;
    endPos = endPos || path[path.length-1].center;

    var portals = [];

    portals.push({
        left: startPos,
        right: startPos
    });

    path.forEach(function (triangle, idx) {
        if (idx==0) return;
        
        var pTriangle = path[idx-1];

        triangle.edges.forEach(function (edge) {
            edge.triangles.forEach(function (t) {
                if (t != pTriangle) return;

                var pointIndices = t.indices.slice(0);
                while (pointIndices[0] == edge.indices[0] || pointIndices[0] == edge.indices[1]) {
                    pointIndices.push(pointIndices.shift());
                }

                portals.push({
                    left: points[pointIndices[2]],
                    right: points[pointIndices[1]],
                });
            });
        });
    });

    portals.push({
        left: endPos,
        right: endPos
    });

    return portals;
}

function funnel(portals) {
    var pts = [];

    // Init scan state
    var portalApex, portalLeft, portalRight;
    var apexIndex = 0;
    var leftIndex = 0;
    var rightIndex = 0;

    portalApex  = portals[0].left;
    portalLeft  = portals[0].left;
    portalRight = portals[0].right;

    // Add start point.
    pts.push(portalApex);

    for (var i = 1; i < portals.length; i++) {
        var left = portals[i].left;
        var right = portals[i].right;

        // Update right vertex.
        if (triarea2(portalApex, portalRight, right) <= 0.0) {
            if (vequal(portalApex, portalRight) || triarea2(portalApex, portalLeft, right) > 0.0) {
                // Tighten the funnel.
                portalRight = right;
                rightIndex = i;
            } else {
                // Right over left, insert left to path and restart scan from portal left point.
                pts.push(portalLeft);
                // Make current left the new apex.
                portalApex = portalLeft;
                apexIndex = leftIndex;
                // Reset portal
                portalLeft = portalApex;
                portalRight = portalApex;
                leftIndex = apexIndex;
                rightIndex = apexIndex;
                // Restart scan
                i = apexIndex;
                continue;
            }
        }

        // Update left vertex.
        if (triarea2(portalApex, portalLeft, left) >= 0.0) {
            if (vequal(portalApex, portalLeft) || triarea2(portalApex, portalRight, left) < 0.0) {
                // Tighten the funnel.
                portalLeft = left;
                leftIndex = i;
            } else {
                // Left over right, insert right to path and restart scan from portal right point.
                pts.push(portalRight);
                // Make current right the new apex.
                portalApex = portalRight;
                apexIndex = rightIndex;
                // Reset portal
                portalLeft = portalApex;
                portalRight = portalApex;
                leftIndex = apexIndex;
                rightIndex = apexIndex;
                // Restart scan
                i = apexIndex;
                continue;
            }
        }
    }

    if ((pts.length === 0) || (!vequal(pts[pts.length - 1], portals[portals.length - 1].left))) {
        // Append last point to path.
        pts.push(portals[portals.length - 1].left);
    }

    this.path = pts;
    return pts;
};

    function triarea2(a, b, c) {
        /*var ax = b.x - a.x;
        var az = b.z - a.z;
        var bx = c.x - a.x;
        var bz = c.z - a.z;*/
        var ax = b.x - a.x;
        var ay = b.y - a.y;
        var bx = c.x - a.x;
        var by = c.y - a.y;

        return bx * ay - ax * by;
    }

    function vequal(a, b) {
        return distanceToSquared(a, b) < 0.00001;
    }

    var distanceToSquared = function (a, b) {
        var dx = a.x - b.x;
        var dy = a.y - b.y;
        var dz = 0 - 0;

        return dx * dx + dy * dy + dz * dz;
    };
