"use strict";

var tolerance = 0.01;
function isPointOnSegment(p, ss, se) {
    // y = mx + b;
    // m == slope;
    // b == y-intercept;
    if (ss.x > se.x) ss = [se, se = ss][0];

    if ( (ss.x < p.x) && (p.x < se.x) ) {
        var m = (se.y - ss.y)/(se.x - ss.y);
        // y = mx + b;
        // b = y-mx;
        var b = se.y-(m*se.x);
        var y = m*p.x + b;
        var dif = Math.abs(y-p.y);
        return dif < tolerance;
    }
}

/*function getPointOfIntersectionBetweenSegments(s1s, s1e, s2s, s2e) {
    if (isPointOnSegment(s1s, s2s, s2e)) return s1s;
    if (isPointOnSegment(s1e, s2s, s2e)) return s1e;

    var s1d = s1e.subtract(s1s);
    var s2d = s2e.subtract(s2s);

    var dx = s2s.x - s1s.x;
    var dy = s2s.y - s1s.y;
    var delta = s2d.x * s1d.y - s2d.y * s1d.x;
    if (delta == 0) return; // Rays are parallel
    var u = (dy * s2d.x - dx * s2d.y)/delta;
    var v = (dy * s1d.x - dx * s1d.y)/delta;
    if (Math.sign(u) != Math.sign(v)) return; // Rays don't intersect

    var p = $V([s1s.x + s1d.x * u, s1s.y + s1d.y * u]);

    var s1Length = s1s.distanceFrom(s1e);
    if (s1s.distanceFrom(p) > s1Length) return // Line segments don't intersect
    var s2Length = s2s.distanceFrom(s2e);
    if (s2s.distanceFrom(p) > s2Length) return // Line segments don't intersect
    
    return p;
}*/

//function doLineSegmentsIntersect(p, p2, q, q2) {
function getPointOfIntersectionBetweenSegments(p, p2, q, q2) {
    //if (isPointOnSegment(p,  q, q2)) return p;
    //if (isPointOnSegment(p2, q, q2)) return p2;

    var r = p2.subtract(p);
    var s = q2.subtract(q);

    var uNumerator = q.subtract(p).cross(r);
    var denominator = r.cross(s);

    if (uNumerator == 0 && denominator == 0) {
        // They are coLlinear
        return;
        
        /*// Do they touch? (Are any of the points equal?)
        if (p.eql(q) || p.eql(q2) || p2.eql(q) || p2.eql(q2)) {
            return true
        }
        // Do they overlap? (Are all the point differences in either direction the same sign)
        // Using != as exclusive or
        return ((q.x - p.x < 0) != (q.x - p2.x < 0) != (q2.x - p.x < 0) != (q2.x - p2.x < 0)) || 
            ((q.y - p.y < 0) != (q.y - p2.y < 0) != (q2.y - p.y < 0) != (q2.y - p2.y < 0));*/
    }

    if (denominator == 0) {
        // lines are paralell
        return;
    }

    var t = q.subtract(p).cross(s) / denominator;
    var u = uNumerator / denominator;

    //return (t >= 0) && (t <= 1) && (u >= 0) && (u <= 1);
    if ( (t >= 0) && (t <= 1) && (u >= 0) && (u <= 1) ) {
        var intersectionPoint = p.add(r.multiply(t));
        //console.log(JSON.stringify(p));
        return intersectionPoint;
    }
}

//var p = getPointOfIntersectionBetweenSegments($V(0, 0), $V(4, -4), $V(4, 0), $V(0, -4));
//var p = getPointOfIntersectionBetweenSegments($V([0, -1]), $V([0, 4]), $V([-2, 0]), $V([2, 0]));
//console.log(p);

//var b = getPointOfIntersectionBetweenSegments($V([0, 2]), $V([0, 0]), $V([-1, 0]), $V([1, 0]));
//console.log(b);
