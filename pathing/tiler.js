var ex = window || module.exports;

ex.extractBodies = function (tilesInput, width, solids, scale) {
    var height = tilesInput.length/width;
    scale = scale || 1;
    var tiles = [];
    for (var x = 0; x < width; ++x) {
        var column = [];
        tiles.push(column);
        for (var y = 0; y < height; ++y) {
            column[y] = tilesInput[(y*width)+x];
        }
    }

    //var width = tiles.length;
    //var height = tiles[0].length;

    var hEdges = [];
    for (var i = 0; i < height+1; ++i) {
        var a = [];
        a.length = width;
        hEdges.push(a);
    }
    var vEdges = [];
    for (var i = 0; i < height; ++i) {
        var a = [];
        a.length = width;
        vEdges.push(a);
    }

    for (var y = 0; y < height; ++y) {
        for (var x = 0; x < width; ++x) {
            var tile = tiles[x][y];
            if (tile < 1) continue;
            // top
            if ( (tiles[x][y-1] || 0) < 1) hEdges[y][x] = {};
            // bottom
            if ( (tiles[x][y+1] || 0) < 1) hEdges[y+1][x] = {};
            // left
            if ( !tiles[x-1] || ((tiles[x-1][y] || 0) < 1)) vEdges[y][x] = {};
            // right 
            if ( !tiles[x+1] || ((tiles[x+1][y] || 0) < 1)) vEdges[y][x+1] = {};
        }
    }

    function seek(shape) {
        var p = shape[shape.length-1];
        // right
        if (p.x < width) {
            var edge = hEdges[p.y][p.x];
            if (edge && !edge.processed) {
                shape.push({x: p.x+1, y: p.y});
                edge.processed = true;
                return seek(shape);
            }
        }
        // down
        if (p.y < height) {
            var edge = vEdges[p.y][p.x];
            if (edge && !edge.processed) {
                shape.push({x: p.x, y: p.y+1});
                edge.processed = true;
                return seek(shape);
            }
        }
        // left
        if (p.x > 0) {
            var edge = hEdges[p.y][p.x-1];
            if (edge && !edge.processed) {
                shape.push({x: p.x-1, y: p.y});
                edge.processed = true;
                return seek(shape);
            }
        }
        // up
        if (p.y > 0) {
            var edge = vEdges[p.y-1][p.x];
            if (edge && !edge.processed) {
                shape.push({x: p.x, y: p.y-1});
                edge.processed = true;
                return seek(shape);
            }
        }
    };

    var shapes = [];
    for (var y = 0; y < height-1; ++y) {
        for (var x = 0; x < width; ++x) {
            var vEdge = vEdges[y][x];
            if (vEdge && !vEdge.processed) {
                var shape = [
                    {x: x, y: y+1},
                    {x: x, y: y},
                ];
                seek(shape);
                shapes.push(shape);
            }
        }
    }

    shapes.forEach(function (shape) {
        shape.forEach(function (point, idx) {
            point.x*=scale; point.y*=scale;
            point.index = idx;
        });
    });

    return shapes;
}
