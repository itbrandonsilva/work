"use strict";

var user = require('./user');

var ex = module.exports;

ex.generate = function (cb) {
    user.getAllUsers(function (err, users) {
        var leaderboard = users
            .map(function (user) {
                return {username: user.username, elo: user.elo};
            })
            .sort(function (a, b) {
                return b.elo - a.elo;
            });
        cb(null, leaderboard);
    });
}
