"use strict";

var querystring = require('querystring');
var cookieParser = require('cookie-parser');
var session = require('./session.js');

var leaderboard = require('./leaderboard.js');

var openid = require('openid');
var relyingParty = new openid.RelyingParty(
    'http://98.116.235.176:3000/verify', // Verification URL (yours)
    //null, // Realm (optional, specifies realm for OpenID authentication)
    'http://98.116.235.176:3000', // Realm (optional, specifies realm for OpenID authentication)
    true, // Use stateless verification
    false, // Strict mode
    []); // List of extensions to enable and include


var express = require('express');
var app = express();

app.use(express.static('public'));
app.use(cookieParser());

app.use(session.restore);

app.get('/user', function (req, res) {
    res.json({user: req.user});
});

app.get('/logout', function (req, res) {
    session.logout(req, res, function (err) {
        if (err) return res.end(err);
        res.redirect('/');
    });
});

app.get('/authenticate', function (req, res) {
    // User supplied identifier
    if (req.user) return res.redirect('/');
    var identifier = req.query.openid_identifier;

    // Resolve identifier, associate, and build authentication URL
    relyingParty.authenticate(identifier, false, function(error, authUrl) {
        if (error)
        {
            res.writeHead(200);
            res.end('Authentication failed: ' + error.message);
        }
        else if (!authUrl)
        {
            res.writeHead(200);
            res.end('Authentication failed');
        }
        else
        {
            res.writeHead(302, { Location: authUrl });
            res.end();
        }
    });
});

app.get('/verify', function (req, res) {
    relyingParty.verifyAssertion(req, function(err, result) {
        if (err) return res.end(err);
        var steamId = result.claimedIdentifier.split('/').pop();
        session.login(steamId, req, res, function (err) {
            if (err) return res.end(err.message);
            res.redirect('/');
        });
    });
});

// age, data
var cachedLeaderboard = [0, null];
app.get('/leaderboard', function (req, res) {
    if ( (new Date().getTime() - leaderboard[0]) < (1000*5) ) return res.json(leaderboard[1]);
    leaderboard.generate(function (err, leaderboard) {
        if (err) return res.end(err.message);
        leaderboard = {leaderboard: leaderboard};
        res.json(leaderboard);
        cachedLeaderboard = [new Date().getTime(), leaderboard];
    });
});

var server = app.listen(3000, function () {
    var host = server.address().address;
    var port = server.address().port;
    console.log('Example app listening at http://%s:%s', host, port);
});
