"use strict";

var redis = require('./redis-db.js').user;
var steam = require('./steam.js');
var _ = require('lodash');

var ex = module.exports;

ex.getUser = function (steamId, cb) {
    redis.get(steamId, function (err, profile) {
        cb(err, (profile ? JSON.parse(profile) : null));
    });
};

ex.getAllUsers = function (cb) {
    redis.keys('*', function (err, keys) {
        if (err) return cb(err);
        redis.mget(keys, function (err, users) {
            if (err) return cb(err);
            users.forEach(function (user, idx) {
                users[idx] = JSON.parse(user);
            });
            cb(null, users);
        }); 
    });
};

ex.refreshUser = function (steamId, cb) {
    ex.getUser(steamId, function (err, currentProfile) {
        if (err) return cb(err);
        steam.resolveSteamId(steamId, function (err, latestProfile) {
            if (err) return cb(err);
            if ( ! latestProfile ) return cb(new Error('Failed to retrieve steam profile: ' + steamId));

            var profile = currentProfile || {};
            _.merge(profile, latestProfile);
            if ( ! profile.elo ) profile.elo = 1000;
            redis.set(steamId, JSON.stringify(profile));
            cb(null, profile);
        });
    });
};
