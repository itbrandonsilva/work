"use strict";

var _ = require('lodash');

var elo = require('./elo.js');
var session = require('./session.js');

var engine = require('engine.io');
var server = engine.listen(8086);

var Handler = function (socket, msg) {
    var split = msg.split('\n');
    var type = split[0];
    var data = split[1];
    switch (type) {
        case 'PLAYER_QUEUE':
            socket.player.status = 'queue';
            socket.player.range = 0;
            break;
        case 'PLAYER_UNQUEUE':
            socket.player.status = 'idle';
            break;
        default:
            console.error('Request dropped:', msg);
            return false;
    }
    return true;
}

function SendPlayerCount(socket) {
    var count = 0;
    for (var socketId of Object.keys(server.clients)) {
        if (server.clients[socketId].player.status == 'queue') count++;
    }
    socket.send('SERVER_COUNT_QUEUE\n' + count);
}

function SendQueueMatch(socket, player) {
    socket.send('SERVER_FOUND_MATCH\n' + JSON.stringify({player: player}));
}

server.on('connection', function (socket) {
    socket.player = {id: null};
    session.restore(socket.request, null, function () {
        socket.on('close', function () {
            console.log('Dropped player:', socket.player.id);
        });
        socket.on('message', function (data) {
            if ( ! Handler(socket, data) ) console.log('Could not handle:', data);
        });

        var user = socket.request.user;
        if ( ! user ) {
            console.log('Received anonymous connection.');
        } else {
            socket.player = {id: user.id, username: user.username, status: 'idle', elo: user.elo, socket: socket};
            console.log('Connection from: ', socket.player.username);
        }
    });
});

setInterval(function () {
    var queuedPlayers = Object.keys(server.clients).map(function (socketId) {
        var socket = server.clients[socketId];
        if (socket.player.status == 'queue') return socket.player;
    }).filter(function (player) { if (player) return true; });

    for (var socketId of Object.keys(server.clients)) {
        var socket = server.clients[socketId];
        SendPlayerCount(socket);
        switch (socket.player.status) {
            case 'queue':
                SendPlayerCount(socket);

                var matchedWith;
                queuedPlayers.some(function (player) {
                    if (socket.player == player) return;
                    var diff = Math.abs(socket.player.elo - player.elo);
                    if ( diff < socket.player.range && diff < player.range ) {
                        matchedWith = player;
                        return true;
                    }
                });

                if (matchedWith) {
                    _.pull(queuedPlayers, socket.player, matchedWith);
                    socket.player.status = 'idle';
                    matchedWith.status = 'idle';

                    SendQueueMatch(socket, matchedWith.username);
                    SendQueueMatch(matchedWith.socket, socket.player.username);
                } else {
                    socket.player.range += 5;
                    console.log(socket.player.username, 'range:', socket.player.range);
                }
                break;
        }
    }
}, 1000);

console.log('Listening...');
