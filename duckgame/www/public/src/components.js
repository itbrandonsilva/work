"use strict";

window.SteamLoginButton = React.createClass({
    getInitialState: function () { return null; },
    render: function () {
        return (
            <a href="/authenticate?openid_identifier=http://steamcommunity.com/openid">
                <img class="steam-login" src="./images/steam-login.png">
                </img>
            </a>
        );
    }
});

window.PlayerCount = React.createClass({
    getInitialState: function () { return null; },
    render: function () {
        return (
            <span className="">
                Players in queue: {this.props.count}
            </span>
        );
    }
});

window.Profile = React.createClass({
    getInitialState: function () { return null; },
    render: function () {
        return (
            <div>
                Logged in as: <b>{this.props.username}</b> <i>Elo: {this.props.elo}</i><br />
                <a href="/logout">Logout</a><br /><br />
            </div>
        );
    }
});

window.Leaderboard = React.createClass({
    getInitialState: function () { return {leaderboard: []}; },
    refresh: function () {
        superagent.get('/leaderboard')
            .end((err, res) => {
                if (err) return console.log(error);
                this.setState({leaderboard: res.body.leaderboard});
            });
    },
    componentDidMount: function () {
        this.refresh();
    },
    render: function () {
        var rows = this.state.leaderboard.map(function (entry, idx) {
            return <tr><td>{idx+1}</td><td>{entry.username}</td><td>{entry.elo}</td></tr>;
        });
        return (
            <div className="leaderboard-container">
                <table className="leaderboard-table">
                    <tr><td>Rank</td><td>User</td><td>Rating</td></tr>
                    {rows}
                </table>
            </div>
        )
    }
});

window.MatchMaker = React.createClass({
    getInitialState: function () { return {}; },
    queue1v1: function () {
        window.Queue1v1();
        this.setState({inQueue: true});
    },
    unqueue: function () {
        window.Unqueue();
        this.setState({inQueue: false});
    },
    foundMatch: function (matchInfo) {
        console.log('Found match!');
        console.log(matchInfo);
    },
    render: function () {
        if (this.state.inQueue) return (
            <div>
                <h3>Currently queued...</h3>
                <button type="button" onClick={this.unqueue}>Unqueue</button>
            </div>

        )

        if (typeof this.props.canQueue == 'undefined') return (
            <h3>You must be logged in to queue.</h3>
        )

        if (this.props.canQueue) return (
            <div>
                <button type="button" onClick={this.queue1v1}>Queue 1v1</button>
            </div>
        )

        return (
            <h3>You must be a part of the Duck Game community group to use the matchmaker. (You also need to have setup your Steam community profile)</h3>
        )
    }
});
