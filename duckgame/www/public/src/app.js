"use strict";

function Handler(data) {
    var split = data.split('\n');
    var type = split[0];
    var msg = split[1];
    switch (type) {
        case 'SERVER_COUNT_QUEUE':
            PlayerCountEl.setProps({count: msg});
            break;
        case 'SERVER_FOUND_MATCH':
            MatchMakerEl.foundMatch(JSON.parse(msg));
            break;
        default:
            console.log('Could not handle:');
            console.log(type);
            console.log(msg);
            break;
    }
}

window.Queue1v1 = function () {
    socket.send('PLAYER_QUEUE\n{}');
    console.log('Attempting to queue...');
}

window.Unqueue = function () {
    socket.send('PLAYER_UNQUEUE\n{}');
    console.log('Attempting to Unqueue...');
}


var socket = eio('ws://98.116.235.176:8086');
socket.on('open', function () {
    socket.on('message', function (data) { Handler(data); });
    socket.on('close', function () { console.log('Connection closed...'); });
});

var PlayerCountEl = ReactDOM.render(<PlayerCount count="0" />, document.getElementById('player-count'));
var MatchMakerEl = ReactDOM.render(<MatchMaker />, document.getElementById('matchmaker'));
var LeaderboardEl = ReactDOM.render(<Leaderboard />, document.getElementById('leaderboard'));
superagent.get('/user')
    .end(function (err, res) {
        if (err) console.error(err);
        var user = res.body.user;
        if ( ! user ) return ReactDOM.render(<SteamLoginButton />, document.getElementById('steam-login'));
        console.log('User:', user);
        ReactDOM.render(<Profile username={user.username} elo={user.elo} />, document.getElementById('player-profile'));
        MatchMakerEl.setProps({canQueue: user.canQueue});
    });

console.log('Running...');
