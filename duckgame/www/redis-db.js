"use strict";

var redis = require('redis');

var dbs = [
    {index: 0, alias: 'user'},
    {index: 1, alias: 'session'}
];

for (var db of dbs) {
    var client = redis.createClient();
    module.exports[db.alias] = client;

    client.select(db.index, function () {});
    client.on('error', function (err) {
        console.log('Redis error:', db.alias);
        console.log(err);
    });
}
