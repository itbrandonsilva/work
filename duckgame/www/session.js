"use strict";

var redis = require('./redis-db.js').session;
var user = require('./user.js');
var cookie = require('cookie');

var COOKIE_ID = 'quack-session';
var SESSION_AGE = 1000*60*60*24;

var ex = module.exports;

// middleware
ex.restore = function (req, res, next) {
    if ( ! req.cookies ) req.cookies = cookie.parse(req.headers.cookie);
    var sessionId = req.cookies[COOKIE_ID];
    if ( ! sessionId ) return next();
    redis.get(sessionId, function (err, steamId) {
        if (err) return res.end(err);
        if ( ! steamId ) return next();
        user.getUser(steamId, function (err, profile) {
            req.user = profile;
            next();
        });
    }); 
}

ex.login = function (steamId, req, res, cb) {
    user.refreshUser(steamId, function (err, profile) {
        if (err) return cb(err);
        var cookie = Math.floor(Math.random()*999999);
        if ( ! profile ) return cb(new Error('Could not get steam profile.'));
        redis.set(cookie, steamId);
        redis.expire(cookie, SESSION_AGE);
        res.cookie(COOKIE_ID, cookie, {maxAge: SESSION_AGE, httpOnly: true});
        cb();
    });
}

ex.logout = function (req, res, cb) {
    var sessionId = req.cookies[COOKIE_ID];
    if ( ! sessionId ) return res.redirect('/'); 
    redis.del(sessionId, function (err) {
        res.clearCookie(COOKIE_ID);
        cb(err);
    });
}
