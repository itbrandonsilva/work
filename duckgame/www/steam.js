"use strict";

var superagent = require('superagent');
var parseXML = require('xml2js').parseString;

//var STEAM_API_KEY = "DEFE6AC2A7F9B41B5CA53C49CD52A9E6";

var ex = module.exports;

ex.resolveSteamId = function (steamId, cb) {
    superagent.get('http://steamcommunity.com/profiles/' + steamId + '/?xml=1')
        .end(function (err, playerProfile) {
            if (err) return cb(err);
            parseXML(playerProfile.text, function (err, result) {
                if (err) return cb(err);
                var profile = {};
                profile.id = steamId;
                var displayName = result.profile.steamID[0];
                profile.username = (displayName.length ? displayName : 'SillyPleb');
                profile.canQueue = false;
                console.log(result.profile.groups);
                if (result.profile.groups) profile.canQueue = result.profile.groups.some(function (group) {
                    return group.group[0].groupID64[0] == '103582791440138173';
                });
                console.log('Profile...');
                console.log(profile);
                cb(err, profile);
            });
        });
}
