import {Engine, Render, World, Bodies, Body, Vector} from 'matter-js';

// create an engine
var engine = Engine.create();
engine.world.gravity.x = 0;
engine.world.gravity.y = 0;

let element = document.getElementById('boids');
// create a renderer
var render = Render.create({
    element: element,
    engine: engine,
});

render.options.wireframes = false;
(<any>render).options.showAngleIndicator = true;

function getRadius(area: number) {
    return Math.sqrt(area/Math.PI)
}

function getArea(radius: number) {
    return Math.PI * (radius * radius);
}

const NUM_AGENTS = 20;
const RADIUS = getRadius(getArea(10) * NUM_AGENTS) + 3
//const RADIUS = 50;
console.log(RADIUS);

class Agent {
    private body: Body;
    public destination: Vector;
    private speed: number = 0.037;
    public arrived: boolean = false;
    public arrivalTS: number;

    constructor(x: number, y: number, radius: number, options?: any, maxSides?: number) {
        options = options || {slop: 0.1};
        let body = Bodies.circle(x, y, radius);
        body.frictionAir = 0.2;
        //body.inertia = body.inverseInertia = Infinity;
        World.add(engine.world, [body]);
        this.body = body;
    }

    public steer(ms: number) {
        let desiredVelocity;
        if (this.arrived) {
            if (new Date().getTime() - this.arrivalTS > 1500) return;
            //desiredVelocity = Vector.div(Vector.add(this.steerCohesion(ms), this.steerSeperation(ms)), 2);
            desiredVelocity = Vector.mult(this.steerCohesion(ms), 0.1);
            //desiredVelocity = this.steerSeperation(ms);
            //desiredVelocity = this.steerSeek(ms);

            //desiredVelocity = this.steerCohesion(ms);
            //Body.applyForce(this.body, this.body.position, desiredVelocity);

            /*
            desiredVelocity = this.steerSeperation(ms);
            Body.applyForce(this.body, this.body.position, desiredVelocity);
            desiredVelocity = Vector.mult(this.steerCohesion(ms), 0.8);
            Body.applyForce(this.body, this.body.position, desiredVelocity);
            */

            //desiredVelocity = Vector.div(Vector.add(this.steerCohesion(ms), this.steerSeperation(ms)), 2);
            Body.applyForce(this.body, this.body.position, desiredVelocity);
        } else {

            desiredVelocity = this.steerSeek(ms);
            Body.applyForce(this.body, this.body.position, desiredVelocity);
        }

        /*desiredVelocity = Vector.add(
            Vector.mult(desiredVelocity, 0.5),
            Vector.mult(this.steerAvoid(ms), 0.5)
        );*/

        //Body.setVelocity(this.body, desiredVelocity);
        //console.log(desiredVelocity);
        //desiredVelocity = Vector.create(0.01, 0);
        //desiredVelocity = Vector.mult(desiredVelocity, ms/1000);
    }

    public setDestination(destination: Vector) {
        this.destination = destination;
        delete this.arrivalTS;
        this.arrived = false;
    }

    private steerSeek(ms: number): Vector {
        if ( ! this.destination ) return Vector.create(0, 0);
        let diff = Vector.sub(this.body.position, this.destination);
        let mag = Vector.magnitude(diff);

        let f;
        agents.some((agent: Agent) => {
            if (agent === this) return;
            if (! agent.arrived) return;

            //let dist = Vector.magnitude(Vector.sub(this.destination, agent.body.position));
            let dist = Vector.magnitude(Vector.sub(this.body.position, agent.body.position));
            if (dist < 22 && mag <= RADIUS) {
                if (agent.arrived) {
                    delete this.destination;
                    this.arrived = true;
                    this.arrivalTS = new Date().getTime();
                    f = Vector.create(0, 0);
                    return true;
                }
            }
        });

        if (f) return f;

        if (mag <= 12) { 
            delete this.destination;
            this.arrived = true;
            this.arrivalTS = new Date().getTime();
            return Vector.create(0, 0);
        }
        let direction = Vector.sub(this.destination, this.body.position);
        direction = Vector.normalise(direction);
        let scaled = Vector.mult(direction, this.speed);
        scaled = Vector.mult(scaled, ms/1000);
        return scaled;
    }

    private steerAvoid(ms: number) {
        let x = 0;
        let v = Vector.create();
        agents.forEach(agent => {
            if (agent === this) return;
            let diff = Vector.sub(this.body.position, agent.body.position);
            let mag = Vector.magnitude(diff) 
            if (mag < 21) {
                diff = Vector.normalise(diff);
                //diff = Vector.mult(diff, this.speed);
                v = Vector.add(v, diff);
                ++x;
            }
        });

        if (x === 0) return v;

        v = Vector.div(v, x);
        v = Vector.mult(v, this.speed);
        v = Vector.mult(v, ms/1000);
        return v;
    }

    private steerCohesion(ms: number) {
        let target = Vector.create();
        let neighbs = 0;
        agents.forEach(agent => {
            if (agent === this) return;

            let diff = Vector.sub(agent.body.position, this.body.position);
            let mag = Vector.magnitude(diff) 
            if (mag > 22 && mag < 29) {
                //diff = Vector.normalise(diff);
                neighbs++;
                //total = Vector.add(total, diff);
                target = Vector.add(target, agent.body.position);
            }
        });

        if (!neighbs) return Vector.create();

        target = Vector.div(target, neighbs);
        //target = Vector.mult(target, this.speed);
        //target = Vector.mult(target, ms/1000);
        let dv = Vector.sub(target, this.body.position);
        dv = Vector.normalise(dv);
        dv = Vector.mult(dv, this.speed);
        dv = Vector.mult(dv, ms/1000);
        return dv;
    }

    private steerSeperation(ms: number) {
        let target = Vector.create();
        let neighbs = 0;
        agents.forEach(agent => {
            if (agent === this) return;

            let diff = Vector.sub(agent.body.position, this.body.position);
            let mag = Vector.magnitude(diff) 
            if (mag < 22) {
                //diff = Vector.normalise(diff);
                neighbs++;
                //total = Vector.add(total, diff);
                let wut = Vector.sub(agent.body.position, this.body.position);
                target = Vector.add(target, wut);
            }
        });

        if (!neighbs) return target;

        target = Vector.div(target, neighbs);
        //target = Vector.mult(target, this.speed);
        //target = Vector.mult(target, ms/1000);
        let dv = Vector.sub(target, this.body.position);
        dv = Vector.mult(target, -1);
        dv = Vector.normalise(dv);
        dv = Vector.mult(dv, this.speed);
        dv = Vector.mult(dv, ms/1000);
        return dv;
    }
}






//var ground = Bodies.rectangle(400, 610, 810, 60, { isStatic: true });




// run the engine
Engine.run(engine);

// run the renderer
Render.run(render);

/*
let rightIsDown = false;
document.addEventListener('keydown', (e) => {
    rightIsDown = true;
});

document.addEventListener('keyup', (e) => {
    rightIsDown = false;
});
*/

document.addEventListener('mousedown', (e) => {
    agents.forEach(agent => agent.setDestination(Vector.create(e.clientX, e.clientY)));
    //agents.forEach(agent => agent.setDestination(100, 100));
});

let agents = [];
for (let x = 0; x < NUM_AGENTS; ++x) {
    let agent = new Agent(100 + (x*25), 50, 10);
    agent.setDestination(Vector.create(400, 400));
    agents.push(agent);
}

let then = new Date().getTime();
function run() {
    let now = new Date().getTime(); 
    let elapsedMS = now - then;
    agents.forEach(agent => agent.steer(elapsedMS));
    then = now;
    requestAnimationFrame(run);
}
run();