"use strict";

var noise = require('./perlin.js');

var WIDTH = 20;
var HEIGHT = 20;

for (var x = 0; x < WIDTH; ++x) {
    for (var y = 0; y < HEIGHT; ++y) {
        console.log(noise.noise.perlin2(x/100, y/100));
    }
}
