var canvas = document.getElementById('view');
var ctx = canvas.getContext('2d');
/*var tiles = [0, 0, 1, 0, 1, 1, 1, 0, 0,
             0, 0, 1, 1, 1, 1, 0, 0, 0,
             0, 0, 0, 1, 0, 0, 0, 1, 1,
             0, 0, 0, 1, 0, 0, 0, 1, 1,
             0, 1, 1, 1, 1, 1, 0, 0, 1,
             0, 0, 0, 1, 1, 1, 1, 1, 1,
             1, 1, 1, 1, 0, 0, 0, 0, 1];*/
var tiles = [0, 1, 1, 1, 1, 1, 1, 0, 0,
    0, 1, 1, 1, 1, 1, 0, 0, 0,
    0, 1, 1, 1, 0, 0, 0, 1, 1,
    0, 1, 1, 1, 0, 0, 0, 1, 1,
    0, 1, 1, 1, 1, 1, 0, 0, 1,
    0, 0, 0, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 0, 0, 0, 0, 1];
var TILE_W = 9;
var TILE_H = 7;
var TILE_PIXEL_WH = 100;
//var PIXEL_W = TILE_W * TILE_PIXEL_WH;
//var PIXEL_H = TILE_H * TILE_PIXEL_WH;
var WIDTH = TILE_W * TILE_PIXEL_WH;
var HEIGHT = TILE_H * TILE_PIXEL_WH;
var shapes = extractBodies(tiles, TILE_W, [], TILE_PIXEL_WH);
var points = shapes[0];
points.forEach(function (point, idx) {
    points[idx] = $V([point.x, point.y]);
});
var edges = {};
function addTriangleToEdge(t, i0, i1) {
    var normal = (function () {
        var p1 = points[i0];
        var p2 = points[i1];
        var direction = p2.subtract(p1);
        var normal = direction.rotate(Math.PI / 2, origin).toUnitVector();
        normal = $V([Math.round(normal.x * 100) / 100, Math.round(normal.y * 100) / 100]);
        return normal;
    }());
    if (i0 > i1)
        i1 = [i0, i0 = i1][0];
    edges[i0] = edges[i0] || {};
    edges[i0][i1] = edges[i0][i1] || { indices: [i0, i1], normal: normal, triangles: [], indicesPoints: [points[i0], points[i1]] };
    var edge = edges[i0][i1];
    if (edge.triangles.indexOf(t) < 0)
        edge.triangles.push(t);
    return edge;
}
;
function getEdge(i0, i1) {
    if (i0 > i1)
        i1 = [i0, i0 = i1][0];
    return edges[i0][i1];
}
console.time('triangles');
var triangles = (function () {
    var formattedPoints = [];
    points.forEach(function (point) {
        formattedPoints.push(point.x);
        formattedPoints.push(point.y);
    });
    var indices = earcut(formattedPoints);
    var triangles = [];
    for (var i = 0; i < indices.length; i += 3) {
        var i0 = indices[i + 0];
        var i1 = indices[i + 1];
        var i2 = indices[i + 2];
        var t = { 0: points[i0], 1: points[i1], 2: points[i2], index: i / 3, indices: [i0, i1, i2], edges: [] };
        var tEdges = [[i0, i1], [i1, i2], [i2, i0]];
        tEdges.forEach(function (edge) {
            t.edges.push(addTriangleToEdge(t, edge[0], edge[1]));
        });
        t.center = $V([(t[0].x + t[1].x + t[2].x) / 3, (t[0].y + t[1].y + t[2].y) / 3]);
        triangles.push(t);
    }
    triangles.forEach(function (t) {
        t.neighbors = (function () {
            var a = [];
            t.edges.forEach(function (edge) {
                edge.triangles.forEach(function (n) {
                    if (n != t)
                        a.push(n);
                });
            });
            return a;
        }());
    });
    return triangles;
}());
console.timeEnd('triangles');
var mode = 'MOVE';
canvas.oncontextmenu = function (e) {
    return false;
};
var paused = false;
document.onkeydown = function (e) {
    if (e.keyCode == 80)
        paused = !paused;
};
var lastClicked = null;
canvas.onmousedown = function (e) {
    lastClicked = $V([e.offsetX, e.offsetY]);
    switch (mode) {
        case 'MOVE':
            if (e.button == 0)
                system.moveGroup(lastClicked, 1);
            else if (e.button == 1)
                system.moveAll(lastClicked);
            else if (e.button == 2)
                system.moveGroup(lastClicked, 0);
            break;
        default:
            break;
    }
};
canvas.onmouseup = function (e) {
    //console.log('mouseup');
};
console.log(triangles);
console.log(edges);
console.log(points);
var system = new AISystem();
system.addAi($V([350, 100]), 2, 9);
system.addAi($V([282, 76]), 2, 9);
system.addAi($V([282, 94]), 2, 9);
system.addAi($V([282, 112]), 2, 9);
system.addAi($V([282, 130]), 2, 9);
//system.addAi($V([282, 74]), 6, 9);
//system.addAi($V([282, 75]), 6, 9);
//system.addAi($V([282, 76]), 6, 9);
//system.addAi($V([282, 77]), 6, 9);
//system.addAi($V([282, 78]), 6, 9);
system.addAi($V([228, 76]), 2, 9);
system.addAi($V([228, 94]), 2, 9);
system.addAi($V([228, 112]), 2, 9);
system.addAi($V([228, 130]), 2, 9);
system.addAi($V([246, 76]), 2, 9);
system.addAi($V([246, 130]), 2, 9);
system.addAi($V([264, 76]), 2, 9);
system.addAi($V([264, 130]), 2, 9);
function doPath() {
    /*var a = Math.floor(Math.random()*triangles.length);
    var b = Math.floor(Math.random()*triangles.length);
    while (a==b) {
        b = Math.floor(Math.random()*triangles.length);
    }*/
    //a = 6;
    //b = 29;
    //console.log("From " + a + " to " + b);
    /*var solution = FindPath(triangles[a], triangles[b]);
    var portals = initPortals(solution.path);
    var path = funnel(portals);
    var aiPath = formatPath(path);
    console.log('!?!?!?');
    console.log(JSON.stringify(aiPath, null, 2));*/
    //console.log(path);
    //var cPosition = vec2.fromValues(triangles[a].center.x, triangles[a].center.y);
    var intId = setInterval(function () {
        //var movements = followPath(cPosition, aiPath, 2) || [];
        //if ( ! movements.length ) clearInterval(intId);
        //movements.forEach(function (movement) {
        //    vec2.add(cPosition, cPosition, movement);
        //});
        if (paused)
            return;
        system.step();
        ctx.fillStyle = "white";
        ctx.fillRect(0, 0, 1000, 1000);
        //ctx.clearRect(20,20,100,50);
        ctx.strokeStyle = "#000000";
        ctx.lineWidth = 1;
        for (var i = 0; i < triangles.length; ++i) {
            var triangle = triangles[i];
            ctx.beginPath();
            ctx.moveTo(triangle[0].x, triangle[0].y);
            ctx.lineTo(triangle[1].x, triangle[1].y);
            ctx.lineTo(triangle[2].x, triangle[2].y);
            ctx.lineTo(triangle[0].x, triangle[0].y);
            ctx.stroke();
            ctx.fillStyle = "black";
            ctx.font = "10px serif";
            ctx.fillText(triangle.index, triangle.center.x - 3, triangle.center.y);
        }
        /*ctx.beginPath();
        ctx.strokeStyle="#FF0000";
        ctx.lineWidth = 5;
        ctx.moveTo(path[0].x, path[0].y);
        path.forEach(function (n, idx) {
            if (idx==0) return;
            ctx.lineTo(n.x, n.y);
        });
        ctx.stroke();*/
        system.AIS.forEach(function (ai) {
            // AI current path
            ctx.lineWidth = 1;
            ctx.strokeStyle = "#0000ff";
            ctx.beginPath();
            ctx.moveTo(ai.position.x, ai.position.y);
            if (ai.pathNodes) {
                ai.pathNodes.forEach(function (pathNode) {
                    ctx.lineTo(pathNode.x, pathNode.y);
                });
            }
            ctx.stroke();
            // AI
            ctx.strokeStyle = "#000000";
            ctx.lineWidth = 2;
            ctx.beginPath();
            ctx.arc(ai.position.x, ai.position.y, ai.radius, 0, 2 * Math.PI);
            ctx.stroke();
            // Collision Mocks with "circle"
            /*ctx.strokeStyle="#ff0000";
            ctx.lineWidth = 1;
            if (ai.mocks) ai.mocks.forEach(function (mock) {
                ctx.beginPath();
                ctx.arc(mock.x, mock.y, ai.radius, 0, 2*Math.PI);
                ctx.stroke();
            });
            ctx.strokeStyle="#00ff00";
            ctx.lineWidth = 1;
            if (ai.circle) ai.circle.forEach(function (radius) {
                ctx.beginPath();
                ctx.arc(ai.position.x, ai.position.y, radius, 0, 2*Math.PI);
                ctx.stroke();
            });*/
            delete ai.circle;
            delete ai.mocks;
            // AI ID
            ctx.fillStyle = "black";
            ctx.font = "10px serif";
            ctx.fillText(ai.id, ai.position.x, ai.position.y);
            // Velocity
            ctx.strokeStyle = "#ff0000";
            ctx.lineWidth = 0.5;
            ctx.beginPath();
            ctx.moveTo(ai.position.x, ai.position.y);
            var velocity = ai.velocity.length(100).add(ai.position);
            ctx.lineTo(velocity.x, velocity.y);
            ctx.stroke();
        });
        system._debugArrivalAreas.forEach(function (area) {
            ctx.beginPath();
            ctx.lineWidth = 2;
            ctx.strokeStyle = "green";
            ctx.rect(area.minx, area.miny, area.maxx - area.minx, area.maxy - area.miny);
            ctx.stroke();
        });
        if (lastClicked) {
            ctx.strokeStyle = "#00ff00";
            ctx.lineWidth = 4;
            ctx.beginPath();
            ctx.arc(lastClicked.x, lastClicked.y, 4, 0, 2 * Math.PI);
            ctx.stroke();
        }
    }, 33);
}
doPath();
/*setInterval(function () {
    doPath();
}, 1000);*/
/script>
    < /body>
    < /html>;
