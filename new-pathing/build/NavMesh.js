"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var tiler_ts_1 = require('./tiler.ts');
var Vector2_ts_1 = require('./Vector2.ts');
var earcut = require('earcut');
var TriangleCorner = (function (_super) {
    __extends(TriangleCorner, _super);
    function TriangleCorner() {
        _super.apply(this, arguments);
    }
    return TriangleCorner;
}(Vector2_ts_1.default));
var NavMesh = (function () {
    function NavMesh(grid, width, size, isSolid) {
        var shapes = tiler_ts_1.default(grid, width, [], size);
        var formattedPoints = [];
        this.points = shapes[0].map(function (point, idx) {
            formattedPoints.push(point.x);
            formattedPoints.push(point.y);
            var corner = new TriangleCorner(point.x, point.y);
            corner.index = idx;
            return corner;
        });
        var points = this.points;
        var triangles = [];
        var indices = earcut(formattedPoints);
        var _loop_1 = function(i) {
            var i0 = indices[i + 0];
            var i1 = indices[i + 1];
            var i2 = indices[i + 2];
            var p0 = points[i0];
            var p1 = points[i1];
            var p2 = points[i2];
            var triangle = { index: i / 3, corners: [p0, p1, p2], sides: [], neighbors: [] };
            triangles.push(triangle);
            triangle.corners.forEach(function (corner) { return corner.triangles.push(triangle); });
            this_1.processSides(triangle);
            var t = triangle;
            triangle.center = new Vector2_ts_1.default((t.corners[0].x + t.corners[1].x + t.corners[2].x) / 3, (t.corners[0].y + t.corners[1].y + t.corners[2].y) / 3);
        };
        var this_1 = this;
        for (var i = 0; i < indices.length; i += 3) {
            _loop_1(i);
        }
        triangles.forEach(function (triangle) {
            var neighbors = [];
            triangle.sides.forEach(function (side) {
                side.triangles.forEach(function (neighbor) {
                    if (triangle !== neighbor)
                        neighbors.push(neighbor);
                });
            });
            triangle.neighbors = neighbors;
        });
        this.triangles = triangles;
    }
    NavMesh.prototype.processSides = function (triangle) {
        var _this = this;
        if (triangle.sides.length)
            return;
        triangle.corners.forEach(function (corner, idx) {
            var p1 = corner;
            var p2 = triangle.corners[idx + 1 > 2 ? 0 : idx + 1];
            var i0 = p1.index;
            var i1 = p2.index;
            _this.sides[i0] = _this.sides[i0] || [];
            _this.sides[i1] = _this.sides[i1] || [];
            var side = _this.sides[p1.index][p2.index];
            if (!side) {
                side = { triangles: [], points: [p1, p2] };
            }
            side.triangles.push(triangle);
            triangle.sides.push(side);
            _this.sides[i0][i1] = side;
            _this.sides[i1][i0] = side;
        });
    };
    NavMesh.prototype.draw = function () {
    };
    return NavMesh;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = NavMesh;
var tiles = [0, 1, 1, 1, 1, 1, 1, 0, 0,
    0, 1, 1, 1, 1, 1, 0, 0, 0,
    0, 1, 1, 1, 0, 0, 0, 1, 1,
    0, 1, 1, 1, 0, 0, 0, 1, 1,
    0, 1, 1, 1, 1, 1, 0, 0, 1,
    0, 0, 0, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 0, 0, 0, 0, 1];
var TILE_W = 9;
var TILE_H = 7;
var TILE_PIXEL_WH = 100;
var WIDTH = TILE_W * TILE_PIXEL_WH;
var HEIGHT = TILE_H * TILE_PIXEL_WH;
var navmesh = new NavMesh(tiles, TILE_W, TILE_PIXEL_WH, null);
navmesh.draw();
