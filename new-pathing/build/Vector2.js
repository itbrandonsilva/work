"use strict";
var Vector2 = (function () {
    function Vector2(x, y) {
        this._v = [x || 0, y || 0];
    }
    Object.defineProperty(Vector2.prototype, "x", {
        get: function () {
            return this._v[0];
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Vector2.prototype, "y", {
        get: function () {
            return this._v[1];
        },
        enumerable: true,
        configurable: true
    });
    Vector2.prototype.clone = function () {
        return new Vector2(this._v[0], this._v[1]);
    };
    Vector2.prototype.add = function (vector) {
        this.addX(vector._v[0]);
        this.addY(vector._v[1]);
        return this;
    };
    Vector2.prototype.addX = function (x) {
        this._v[0] += x;
        return this;
    };
    Vector2.prototype.addY = function (y) {
        this._v[1] += y;
        return this;
    };
    Vector2.prototype.subtract = function (vector) {
        this._v[0] -= vector._v[0];
        this._v[1] -= vector._v[1];
        return this;
    };
    Vector2.prototype.mult = function (scale) {
        this._v[0] *= scale;
        this._v[1] *= scale;
        return this;
    };
    Vector2.prototype.div = function (scale) {
        this._v[0] /= scale;
        this._v[1] /= scale;
        return this;
    };
    Vector2.prototype.isEqual = function (vector) {
        return (this._v[0] == vector._v[0] && this._v[1] == vector._v[1]);
    };
    Vector2.prototype.setLength = function (scale) {
        return this.normalize().mult(scale);
    };
    Vector2.prototype.getLength = function () {
        return Math.sqrt(Math.pow(this._v[0], 2) + Math.pow(this._v[1], 2));
    };
    Vector2.prototype.normalize = function () {
        return this.div(this.getLength());
    };
    Vector2.prototype.distance = function (vector) {
        return vector.clone().subtract(this).getLength();
    };
    Vector2.prototype.angle = function () {
        return Math.atan2(this._v[0], this._v[1]) * (180 / Math.PI);
    };
    Vector2.prototype.round = function () {
        this._v[0] = Math.round(this._v[0]);
        this._v[1] = Math.round(this._v[1]);
        return this;
    };
    Vector2.prototype.rotate = function (degrees, round) {
        round = round || 1;
        var theta = (Math.PI / 180) * degrees;
        var cs = Math.cos(theta);
        var sn = Math.sin(theta);
        var px = this._v[0] * cs - this._v[1] * sn;
        var py = this._v[0] * sn + this._v[1] * cs;
        this._v[0] = px;
        this._v[1] = py;
        return this;
    };
    Vector2.prototype.toArray = function () {
        return this._v.slice();
    };
    Vector2.prototype.log = function () {
        console.log(this._v[0], this._v[1]);
    };
    return Vector2;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Vector2;
