"use strict";
var extractBodies = function (tilesInput, width, solids, scale) {
    var height = tilesInput.length / width;
    scale = scale || 1;
    var tiles = [];
    for (var x = 0; x < width; ++x) {
        var column = [];
        tiles.push(column);
        for (var y = 0; y < height; ++y) {
            column[y] = tilesInput[(y * width) + x];
        }
    }
    var hEdges = [];
    for (var i = 0; i < height + 1; ++i) {
        var a = [];
        hEdges.push(a);
    }
    var vEdges = [];
    for (var i = 0; i < height; ++i) {
        var a = [];
        vEdges.push(a);
    }
    for (var y = 0; y < height; ++y) {
        for (var x = 0; x < width; ++x) {
            var tile = tiles[x][y];
            if (tile < 1)
                continue;
            if ((tiles[x][y - 1] || 0) < 1)
                hEdges[y][x] = {};
            if ((tiles[x][y + 1] || 0) < 1)
                hEdges[y + 1][x] = {};
            if (!tiles[x - 1] || ((tiles[x - 1][y] || 0) < 1))
                vEdges[y][x] = {};
            if (!tiles[x + 1] || ((tiles[x + 1][y] || 0) < 1))
                vEdges[y][x + 1] = {};
        }
    }
    function seek(shape) {
        var p = shape[shape.length - 1];
        if (p.x < width) {
            var edge = hEdges[p.y][p.x];
            if (edge && !edge.processed) {
                shape.push({ x: p.x + 1, y: p.y });
                edge.processed = true;
                return seek(shape);
            }
        }
        if (p.y < height) {
            var edge = vEdges[p.y][p.x];
            if (edge && !edge.processed) {
                shape.push({ x: p.x, y: p.y + 1 });
                edge.processed = true;
                return seek(shape);
            }
        }
        if (p.x > 0) {
            var edge = hEdges[p.y][p.x - 1];
            if (edge && !edge.processed) {
                shape.push({ x: p.x - 1, y: p.y });
                edge.processed = true;
                return seek(shape);
            }
        }
        if (p.y > 0) {
            var edge = vEdges[p.y - 1][p.x];
            if (edge && !edge.processed) {
                shape.push({ x: p.x, y: p.y - 1 });
                edge.processed = true;
                return seek(shape);
            }
        }
    }
    ;
    var shapes = [];
    for (var y = 0; y < height - 1; ++y) {
        for (var x = 0; x < width; ++x) {
            var vEdge = vEdges[y][x];
            if (vEdge && !vEdge.processed) {
                var shape = [
                    { x: x, y: y + 1 },
                    { x: x, y: y },
                ];
                seek(shape);
                shapes.push(shape);
            }
        }
    }
    shapes.forEach(function (shape) {
        shape.forEach(function (point, idx) {
            point.x *= scale;
            point.y *= scale;
            point.index = idx;
        });
    });
    return shapes;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = extractBodies;
