/*
 Represents a path that was found using FindPath().
*/

function Path () {
    this.tilePath     = [];
    this.checkedTiles = [];
};

/*
 HCost:
 "the estimated movement cost to move from that given square on 
 the grid to the final destination, point B. This is often referred to 
 as the heuristic, which can be a bit confusing. The reason why it is 
 called that is because it is a guess. We really don’t know the actual distance
 until we find the path, because all sorts of things can be in the way 
 (walls, water, etc.). You are given one way to calculate H in this tutorial, 
 but there are many others that you can find in other articles on the web."
 The HCost represents the <i>estimated</i> cost to get from the this tile to the end tile.
*/

/*
 GCost:
 "the movement cost to move from the starting point A to a given square on the grid, following the path generated to get there."
 The GCost represents the <i>real</i> cost to get from the starting tile to this tile.
*/

/*
 FCost:
 GCost + HCost
*/

function dst(na, nb) {
    var c0 = na.center;
    var c1 = nb.center;
    var ax = c0.x;
    var ay = c0.y;
    var bx = c1.x;
    var by = c1.y;
    return Math.sqrt(Math.pow(bx-ax, 2), Math.pow(by-ay, 2));
};

function FindPath (startNode, endNode, fHcost) {

    var idIndex = 0;
    var costs = function (node, pNode) {
        node.hcost = dst(node, endNode);
        node.gcost = (pNode ? pNode.gcost + dst(node, pNode) : 0);
        node.fcost = node.hcost + node.gcost;
        if (node.id) return;
        node.id  = idIndex++;
        node.pid = (pNode ? pNode.id : idIndex);
        (pNode ? node.p = pNode : null);
    };

    function cleanUp(node) {
        delete node.hcost;
        delete node.gcost;
        delete node.fcost;
        delete node.id;
        delete node.pid;
        delete node.p;
    };

    function cleanUpAll() {
        OpenList.forEach(cleanUp);
        ClosedList.forEach(cleanUp);
    };

    // Tiles that have yet to be traversed for a valid path.
    var OpenList   = [];
    // Tiles that we have already traversed in search for a valid path.
    // The closed list does <i>not</i> represent tiles that are not
    // part of a valid path, only tiles that we have already processed. 
    var ClosedList = [];

    costs(startNode);
    OpenList.push(startNode);

    while (true) {

        var parentNode = null;

        // No more tiles to check? Path to be found does not exist.
        if (OpenList.length <= 0) break;

        // Find the tile with the lowest FCost in the open list, as that tile has the 
        // best chance of being being part of the final path (if a path is found). We 
        // will continue our search for a complete path through this tile.
        for (var i = 0; i < OpenList.length; ++i) {
            var node = OpenList[i]
            if (!parentNode) parentNode = node;
            else if (node.fcost < parentNode.fcost) parentNode = node;
        }

        // Move the chosen tile (parentTile) from the OpenList to the ClosedList, as after we are done 
        // processing this tile, we do not want to process it again.
        OpenList.splice(OpenList.indexOf(parentNode), 1);
        ClosedList.push(parentNode);

        // Did we succesfully find a path from the start tile to the end tile?
        if (parentNode == endNode) {
            var solution = {};
            var path = [];
            var node = parentNode;
            while (node) {
                path.push(node);
                if (node.index == startNode.index) {
                    node = null;
                    continue;
                }
                if (node.p) node = node.p;
            }
            path.reverse();
            solution.path = path;

            cleanUpAll();
            return solution;
        }

        // We prepare tiles adjacent to the tile we chose above (parentTile) for processing. 
        parentNode.neighbors.forEach(function (node) {
            // Have we processed this tile already?
            if (ClosedList.indexOf(node) > -1) return;

            // Have we encountered this tile earlier, but have not processed it yet? If so, lets update that tile if necessary.
            var tileIndex = OpenList.indexOf(node);
            if (tileIndex > -1) {
                // Is our new path to this tile faster than the previous path?
                var gcost = node.gcost + dst(node, parentNode);
                var node = OpenList[tileIndex];
                if (gcost < node.gcost) {
                    node.pid = parentNode.id;
                    node.p = parentNode;
                    node.gcost = gcost;
                    node.fcost = gcost + node.hcost;
                }
            } else {
                // This tile will need to be processed; we have yet to encounter this tile in our search.
                costs(node, parentNode);
                OpenList.push(node);
            }
        });
    }

    // No path was found.
    cleanUpAll();
    return null;
};


/* Uncomment to test.
var impassableTerrain = [1];
var TestBoard = [
    [0, 0, 0, 0], 
    [1, 1, 1, 0], 
    [1, 1, 1, 0], 
    [0, 0, 0, 0]
];
var path = FindPath(TestBoard, 0, 0, 3, 0, impassableTerrain);
if (path) console.log(JSON.stringify(path, null, 2));
else console.log("No path was found.");
*/
