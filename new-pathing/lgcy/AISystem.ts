//import Vector2 from './Vector2.ts';
//import NavMesh, { PointPath } from './NavMesh.ts';

/*
export class Agent {
    position: Vector2;
    velocity: Vector2;
    radius: number;
    id: number = null;
    private path: PointPath;

    constructor(position?: Vector2, radius: number = 8, id?: number) {
        this.position = position || new Vector2();
        this.velocity = new Vector2();
        this.radius = radius;
    }

    step() {
        this.position.add(this.velocity);
    }

    getPath() {
        return this.path;
    }

    setPath(path: PointPath) {
        this.path = path;
    }

    resetVelocity() {
        this.velocity.set(0, 0);
    }

    steer(ms: number): void {
        this.resetVelocity();
    }
}
*/

/*
export default class AISystem {
    agents: Array<Agent> = [];
    selected: Array<Agent> = [];
    nextId: number = 1;
    //groups: Array<Array<Agent>> = [];
    navMesh: NavMesh;

    constructor(navMesh: NavMesh) {
        this.navMesh = navMesh
    }

    addAgent(agent: Agent) {
        agent.id = this.newId();
        this.agents.push(agent);
    }

    newId() {
        return this.nextId++;
    }

    //selectRect(topLeft: Vector2, bottomRight: Vector2) {
    //    this.clearSelection();
    //    this.agents.forEach(agent => {
    //        agent.position.inRect(topLeft, bottomRight) ? this.selected.push(agent) : null;
    //    });
    //}

    selectAll() {
        this.selected.length = 0;
        this.agents.forEach(agent => this.selected.push(agent));
    }

    assignPathToSelection(target: Vector2) {
        this.selected.forEach(agent => this.assignPathToAgent(agent, target));
    }

    assignPathToAgent(agent: Agent, target: Vector2) {
        let path = this.navMesh.findPath(agent.position, target);
        if ( ! path ) return;
        agent.setPath(path);
    }

    clearSelection() {
        this.selected.length = 0;
    }

    draw() {
        this.navMesh.draw();
        this.agents.forEach(agent => agent.draw());
    }

    step() {
        this.agents.forEach(agent => agent.step());
    }
}
*/


/*///////////////////

function AISystem() {
    this.AIS = [];
    this.selected = [];
    this.groupIdPool = 1;
    this.groups = [];
}

AISystem.prototype.addAi = function (position, speed, radius) {
    var ai = new AI(position, speed, radius, this);
    this.AIS.push(ai);
    return ai;
}

AISystem.prototype.moveGroup = function (destination, aiGroup) {
    var self = this;
    if ( ! Array.isArray(aiGroup)) aiGroup = [aiGroup];
    var group = {ais: [], destination: destination};
    aiGroup.forEach(function (ai, idx) {
        if (typeof ai == 'number') ai = self.AIS[ai];
        //if (idx != 0) return;
        ai.makePath(destination, self.groupIdPool);
        ai.resetVelocity();
        group.ais.push(ai);
    });
    this.groups[this.groupIdPool] = group;
    this.groupIdPool++;
}

AISystem.prototype.moveAll = function (destination) {
    this.moveGroup(destination, this.AIS);
};

AISystem.prototype.handleArrivalGroups = function () {
    var self = this;

    this._debugArrivalAreas = [];

    this.groups.forEach(function (group) {
        var minx = PIXEL_W;
        var maxx = 0;
        var miny = PIXEL_H;
        var maxy = 0;

        var ais = group.ais.filter(function (ai) {
            return !ai.velocity.length();
        });

        if ( ! ais.length ) return;

        ais.forEach(function (ai) {
            var p = ai.position;
            if (p.x < minx) minx = p.x;
            if (p.x > maxx) maxx = p.x;
            if (p.y < miny) miny = p.y;
            if (p.y > maxy) maxy = p.y;
        });


        var center = $V([ (minx + maxx)/2 , (miny + maxy)/2 ]);
        var closestDistance = $V([minx, miny]).distanceFrom(center);
        var leader;
        ais.forEach(function (ai) {
            var d = ai.position.distanceFrom(center);
            if (d < closestDistance) {
                closestDistance = d;
                leader = ai;
            }
        });

        if ( ! leader ) return;

        leader.velocity = center.subtract(leader.position).length(leader.speed);
        leader.arrive();
        ais.forEach(function (ai) {
            if (ai == leader) return;
            ai.velocity = ai.seek(ai.position.add(leader.velocity));
            ai.avoid();
            ai.arrive();
        });

        //maxx = maxx - minx;
        //minx = 0;
        //maxy = maxy - miny;
        //miny = 0;

        var area = {minx: minx, maxx: maxx, miny: miny, maxy: maxy};
        console.log(area);
        self._debugArrivalAreas.push(area);

    });
};

AISystem.prototype.step = function () {
    this.AIS.forEach(function (ai) {
        //ai.nullVelocity();
    });

    this.AIS.forEach(function (ai) {
        ai.followPath();
    });

    this.AIS.forEach(function (ai) {
        //ai.avoid();
        ai.clipVelocity();
        //ai.wallSlide();
        //ai.dontSink();
    });

    this.AIS.forEach(function (ai) {
        ai.wallSlide();
    });

    this.AIS.forEach(function (ai) {
        ai.seperate();
        ai.clipVelocity();
    });

    this.AIS.forEach(function (ai) {
        ai.arrive();
    });

    this.handleArrivalGroups();

    this.AIS.forEach(function (ai) {
        ai.applyVelocity();
    });
}

function Path(end, groupId) {
    this.end = end;
    this.groupId = groupId;
}

function plan(start) {
    var fromTriangle;
    var toTriangle;

    if (start.eql(this.end)) return;

    for (var i = 0; i < triangles.length; ++i) {
        var t = triangles[i];
        if (PointInTriangle(start, t[0], t[1], t[2])) {
            fromTriangle = triangles[i];
        }
        if (PointInTriangle(this.end, t[0], t[1], t[2])) {
            toTriangle = triangles[i];
        }
        if ( fromTriangle && toTriangle ) break;
    }

    if ( fromTriangle == undefined || toTriangle == undefined ) return;
  
    var solution = FindPath(fromTriangle, toTriangle);
    var portals = initPortals(solution.path, start, this.end);
    var path = funnel(portals);
    return getPathNodes(path, this.groupId);
}

Path.prototype.replan = plan;
Path.prototype.plan   = plan;

var aiIdPool = 1;
var AI = function (position, speed, radius, aiSystem) {
    this.position = position;
    this.velocity = $V([0, 0]);
    this.speed = speed || 5;
    this.path = null;
    this.radius = radius || 5;
    this.aiSystem = aiSystem;
    this.group = null;
    this.id = aiIdPool;
    aiIdPool++;
}

AI.prototype.makePath = function (destination, groupId) {
    var path = new Path(destination, groupId);
    var pathNodes = path.plan(this.position);
    if (pathNodes) {
        this.path = path;
        this.pathNodes = pathNodes;
        this._debugDestination = pathNodes[0];
    }
}

AI.prototype.clearPath = function () {
    console.log('clearPath()');
    delete this.path;
    delete this.pathNodes;
    delete this._debugPath;
};

AI.prototype.nullVelocity = function () {
    this.velocity = $V([0, 0]);
}

AI.prototype.resetVelocity = function () {
    var target = this.getCurrentPathTarget();
    this.velocity = target.subtract(this.position).length(this.speed);
    console.log(this.velocity.elements);
};

AI.prototype.getCurrentPathTarget = function () {
    if ( ! this.pathNodes || ! this.pathNodes.length ) return;
    return this.pathNodes[0];
}

AI.prototype.distanceFromPathTarget = function () {
    var target = this.getCurrentPathTarget();
    if ( ! target ) return 99999999;
    return this.position.subtract(target).length();
}

var ARRIVAL_THRESHOLD = 30;
AI.prototype.arrive = function () {
    if ( ! this.velocity.length() ) return;
    var distance = this.distanceFromPathTarget();

    if (distance < ARRIVAL_THRESHOLD) return this.clearPath();

    if (distance > this.velocity.length()) return;

    console.log(distance);
    if (this.position.eql(this.getCurrentPathTarget)) return this.clearPath();
    this.velocity = this.velocity.length(distance);
}

AI.prototype.seek = function (target) {
    return target.subtract(this.position).length(this.speed);
};

AI.prototype.followPath = function () {
    var path = this.pathNodes;
    if ( ! path ) return this.nullVelocity();
    var target = path[0];
    if ( ! target ) return; // console.log('Figure out why a path exists with no target');
    var steering = this.seek(target);

    if (this.velocity.length == 0) this.velocity = steering;
    else {
        //console.log('steer');
        this.velocity = this.velocity.add(steering.multiply(0.6)).length(this.speed);
        //this.velocity = steering.length(this.speed);
    }

    this.avoid();

    // We will just seek
    return;
}

AI.prototype.getFuturePosition = function (length) {
    length = length || this.velocity.length();
    if ( ! this.velocity || ! this.velocity.length() ) return this.position;
    return this.position.add(this.velocity.length(length));
}

AI.prototype.seperate = function () {
    var AIS = this.aiSystem.AIS;
    var ai = this;
    //AIS.forEach(function (ai) {
        if ( ! ai.velocity.length() ) return;
        var futurePosition = ai.getFuturePosition();

        var force = $V([0, 0]);
        AIS.forEach(function (otherAi) {
            if (otherAi == ai) return;

            // Is the otherAi even moving?
            //if ( ! otherAi.velocity.length() ) return;

            // Is the otherAi behind me
            var relativePosition = otherAi.position.subtract(ai.position);
            relativePosition = ai.position.subtract(otherAi.position);
            //var angle = relativePosition.angleFrom(ai.velocity);
            //if ( angle > Math.PI/2 ) return;

            // We gotta avoid the otherAi
            var minimumDistance = ai.radius + otherAi.radius;
            var otherFuturePosition = otherAi.getFuturePosition();
            var distanceAfterMovement = futurePosition.distanceFrom(otherFuturePosition);
            if ( distanceAfterMovement >= minimumDistance ) return;

            var strength = ai.speed * (minimumDistance - distanceAfterMovement) / minimumDistance;
            
            var f = relativePosition.toUnitVector().multiply(strength);
            force = force.add(f);
        });

        //forces.forEach(function (f) {
        //    ai.velocity = ai.velocity.add(f);
        //    if (ai.velocity.length > ai.speed) {
        //        ai.velocity = ai.velocity.toUnitVector().multiply(ai.speed);
        //    }
        //});

        ai.velocity = ai.velocity.add(force);
        if (ai.velocity.length() > ai.speed) ai.velocity = ai.velocity.toUnitVector().multiply(ai.speed);

        if (ai.path) {
            ai.schedulePath = true;
        }
    //});

    //AIS.forEach(function (ai) {
    //    var forces = [];
    //    for (var i in edges) {
    //        for (var j in edges[i]) {
    //            var edge = edges[i][j];
    //            if (edge.triangles.length == 1) {
    //                var p1 = points[edge.indices[0]];
    //                var p2 = points[edge.indices[1]];
    //                var point = getPointOfIntersectionBetweenSegments(ai.position, ai.getFuturePosition(), p1, p2);
    //                if (point) {
    //                    var distanceFromWall = ai.position.distanceFrom(point);
    //                    var relativePosition = ai.position.subtract(point);

    //                    var strength = ai.speed * (0.1 - distanceFromWall) / 0.1;
    //                    forces.push(edge.normal.multiply(0.1));
    //                }
    //            }
    //        }
    //    }

    //    forces.forEach(function (f) {
    //        ai.velocity = ai.velocity.add(f);
    //        if (ai.velocity.length > ai.speed) {
    //            ai.velocity = ai.velocity.toUnitVector().multiply(ai.speed);
    //        }
    //    });
    //});

};

//AI.prototype.avoid = function () {
//    var self = this;
//    var ai = this;
//
//    // Don't avoid anyone if we are not moving.
//    if (ai.velocity.length() == 0) return;
//    //1. Find the target that’s closest to collision
//
//    // Store the first collision time
//    var shortestTime = Number.POSITIVE_INFINITY; // inf
//    //var shortestDistance = 9999999; // inf
//
//    var firstTarget;
//    var firstMinSeparation;
//    var firstDistance;
//    var firstRelativePos;
//    var firstRelativeVel;
//
//    var candidates = [];
//
//    self.aiSystem.AIS.some(function (otherAi) {
//        if (otherAi == ai) return;
//
//        // Calculate the time to collision
//        var relativePos = otherAi.position.subtract(ai.position); // 4, 4
//        var relativeVel = otherAi.velocity.subtract(ai.velocity); // 4, 4
//        var distance = relativePos.length()
//        var relativeSpeed = relativeVel.length(); // 5.656
//
//        // We only consider targets that are within 100 units from us
//        if (distance > 100) return;
//
//        // This isn't really a timeToCollision, it's the time where the otherAi and ai will be closest
//        var timeToCollision = -(relativePos.dot(relativeVel) / (relativeSpeed * relativeSpeed));
//        if (timeToCollision <= 0) {
//            //console.log('Moving away...');
//            //console.log(relativePos.dot(relativeVel), ai.velocity.elements, otherAi.velocity.elements);
//            return; // console.log('skip time');
//        }
//
//        var fp = ai.position.add(ai.velocity.multiply(timeToCollision));
//        var ofp = otherAi.position.add(otherAi.velocity.multiply(timeToCollision));
//
//        // Check if it is going to be a collision at all
//        //var minSeparation = (distance - relativeSpeed) * timeToCollision;
//        var minSeparation = fp.distanceFrom(ofp);
//        if (minSeparation > ai.radius*2) return; //console.log('skip minsep');
//        //console.log('-------------');
//        //console.log(relativePos.elements);
//        //console.log(relativeVel.elements);
//        //console.log(distance);
//        //console.log(relativeSpeed);
//        //console.log(minSeparation);
//        //return console.log(otherAi.position.distanceFrom(ai.position));
//
//        console.log(timeToCollision, ' (', otherAi.id, ')');
//
//        // Check if it is the shortest
//        // if ( !shortestTime || (timeToCollision < shortestTime) ) {
//            shortestTime = timeToCollision
//            firstTarget = otherAi;
//            firstMinSeparation = minSeparation;
//            firstDistance = distance;
//            firstRelativePos = relativePos;
//            firstRelativeVel = relativeVel;
//        }
//    });
//
//    // 2. Calculate the steering
//
//    // If we have no target, then exit
//    if ( ! firstTarget ) return delete ai.avoidSign;
//    
//    // If our target is closer than the otherAi, ignore the otherAi
//    var distanceFromOtherAI = firstTarget.position.distanceFrom(ai.position);
//    var d = ai.distanceFromPathTarget();
//    if ( distanceFromOtherAI > d ) return;
//
//
//    console.log("WE GUN COLLIDE", firstTarget.id);
//
//    var relativePos;
//
//    // If we’re going to hit exactly, or if we’re already
//    // colliding, then do the steering based on current
//    // position.
//    // Handles the rare case of colliding at an exact point
//    // Also handles the rare case of two bodies already colliding
//
//
//    if ( firstDistance < 2*ai.radius ) {
//        // We are already colloding, so let's move AWAY from our target
//        //relativePos = firstTarget.position.subtract(ai.position);
//        //console.log('IM INSIDE', firstTarget.id);
//        relativePos = ai.position.subtract(firstTarget.position);
//    } else {
//        // Otherwise calculate the future relative position
//        // THIS IS GOOD relativePos = firstRelativePos.add(firstRelativeVel).multiply(shortestTime);
//        //console.log('IM OUTSIDE', firstTarget.id);
//        var newVel = firstRelativePos.rotateDistance(ai.radius*2);
//        //console.log(ai.velocity.elements);
//        ai.avoidSign = ai.velocity.sign(newVel);
//        return ai.velocity = newVel;
//    }
//
//    relativePos = relativePos.toUnitVector();
//    ai.velocity = ai.velocity.add(relativePos.multiply(ai.speed));
//
//    //var fp = ai.position.add(ai.velocity
//
//    // Avoid the target
//    //relativePos = relativePos.toUnitVector().multiply(ai.speed);
//    //console.log(relativePos.elements);
//
//    // if (firstTarget.velocity.length() == 0) {
//    //     var fp = ai.getFuturePosition();
//    //     var pointToAvoid = firstTarget.position;
//    //     var awayVector = fp.subtract(pointToAvoid).toUnitVector().multiply(ai.radius*2);
//    //     var newVel = awayVector;
//    //     return ai.velocity = newVel;
//    //     var newVel = aiVelocity.subtract(awayFrom).toUnitVector().multiply(ai.speed);
//    //     ai.velocity = newVel;
//    // }
//
//};

AI.prototype.getCollisionInfo = function (other, velocity) {
    velocity = velocity || this.velocity;
    var rpos = other.position.subtract(this.position);  // Relative position
    var rvel = other.velocity.subtract(velocity);       // Relative velocity
    var rs   = rvel.length();                           // Relative speed

    // This isn't really a timeToCollision, it's the time where the other ai and me will be closest
    var timeToCollision = -(rpos.dot(rvel) / (rs * rs));
    if (timeToCollision <= 0) return; // I forgot what this means... check the book

    var fp = this.position.add(velocity.multiply(timeToCollision));
    var ofp = other.position.add(other.velocity.multiply(timeToCollision));
    var separation = fp.distanceFrom(ofp);

    return {
        ai: other,
        rpos: rpos,
        rvel: rvel,
        msep: separation,
        rs: rs,
        time: timeToCollision
    }
}

function findClearPath(start, collisionInfos, sign) {
    // We will need to check for walls at some point, here
    sign = sign || -1;

    var result
    var minAngle = Math.PI;
    //console.log('-------------');
    collisionInfos.forEach(function (ci) {
        if (ci.ai == start) return;
        //console.log('---');
        //console.log(start.velocity.elements);
        //console.log(ci.rpos.elements);
        var maxAngle = ci.rpos.rotateDistance(ci.ai.radius*3).angleFrom(ci.rpos);
        if (start.velocity.angleFrom(ci.rpos) > maxAngle) {
            var osign = ci.rpos.sign(start.velocity);
            if (osign != sign) return //console.log('Sign mismatch:', osign, 'ID:', ci.ai.id); // Sign mismatch
        }

        // Multiplying by 3 for no reason in particular other than multiplying by 2 was not good enough
        var velocity = ci.rpos.rotateDistance(start.radius*3, sign).length(start.speed);

        //var newDistance = velocity.distanceFrom(start.velocity.length(velocity.length()));
        //var newDistance = velocity.distanceFrom(ci.rpos);

        // With this new velocity, are we colliding with anyone else?
        var ci2;
        var collision = collisionInfos.some(function (ci) {
            ci2 = start.getCollisionInfo(ci.ai, velocity);
            if ( ! ci2 ) return;
            // Check if it is going to be a collision at all
            if (ci2.msep > start.radius*2) return; // console.log('We should be far enough (no collision)');
            return true;
        });
        if (collision) {
            if ( ! start.mocks ) start.mocks = [];
            start.mocks.push(start.position.add(velocity.multiply(ci2.time)));
            if ( ! start.circle ) start.circle = [];
            start.circle.push(ci2.rpos.length());
            start.circle.push(ci2.rpos.length());
            //return console.log('We still colliding:', start.position.elements, ci2.ai.id, ci2.msep, newDistance); // Clear path not found around this collision
            //console.log(start.position.elements);
            //console.log(start.velocity.elements);
            //console.log(velocity.elements);
            return;
        }
        var angleFrom = sign * velocity.angleFrom(start.velocity);
        //console.log('We clear with: ', angleFrom);
        if (angleFrom < minAngle) {
            minAngle = angleFrom;
            result = velocity.length(start.speed);
        }
    });
    //console.log('AngleFrom:', minAngle, '(' + sign + ')');

    return result;
}

AI.prototype.avoid = function () {
    var self = this;
    var ai = this;

    // Nothing to do here if we are not moving.
    if (ai.velocity.length() == 0) return;

    var otherAis = [];
    var colliding = false;
    self.aiSystem.AIS.forEach(function (otherAi) {
        if (otherAi == ai) return;

        var collisionInfo = ai.getCollisionInfo(otherAi);
        if ( ! collisionInfo ) return;

        otherAis.push(collisionInfo);

        // Check if there is going to be a collision at all
        if (collisionInfo.msep > ai.radius*2) return; //console.log('skip minsep');

        // If our target is closer than the otherAi, ignore the otherAi
        var distanceFromOtherAI = ai.position.distanceFrom(collisionInfo.ai.position);
        var d = ai.distanceFromPathTarget();
        if ( distanceFromOtherAI > d ) return;

        // If the collision is in the far future, we can ignore it.
        if (collisionInfo.time > 60) return;
        colliding = true;
    });

    // Nothing to avoid?
    if ( ! otherAis.length ) return;
    if ( ! colliding ) return;

    //console.log('---------');
    // left
    var l = findClearPath(this, otherAis);
    //if (l) console.log('l', l.elements, 'a:', l.angleFrom(ai.velocity));
    //else console.log('No solution left');
    // right
    var r = findClearPath(this, otherAis, 1);
    //if (r) console.log('r', r.elements, 'a:', r.angleFrom(ai.velocity));
    //else console.log('No solution right');

    //console.log(ai.velocity.elements);

    // Select the solution that requires the least movement
    var result;
    if ( l && r ) {
        if (l.angleFrom(ai.velocity) < r.angleFrom(ai.velocity)) result = l;
        else result = r;
    } else if ( l || r ) result = l || r;
    else return console.log('No good solution');

    //console.log(result.elements);
    //ai.velocity = result;
    ai.velocity = ai.velocity.add(result.multiply(0.7)).length(ai.velocity.length());
};

AI.prototype.clipVelocity = function () {
    if (this.velocity.length() > this.speed) this.velocity = this.velocity.length(this.speed);
};

AI.prototype.wallSlide = function () {
    if ( ! this.velocity.length() ) return;

    var closestDistance = 99999999;
    var closestEdge;
    for (var i in edges) {
        for (var j in edges[i]) {
            var edge = edges[i][j];
            if (edge.triangles.length == 1) {
                var p1 = points[edge.indices[0]];
                var p2 = points[edge.indices[1]];
                var fp = this.getFuturePosition(100);
                var point = getPointOfIntersectionBetweenSegments(this.position, fp, p1, p2);
                if (point) {
                    var distanceFromWall = this.position.distanceFrom(point);
                    if (distanceFromWall < closestDistance) {
                        closestDistance = distanceFromWall;
                        closestEdge = edge;
                    }
                }
            }
        }
    }

    // Nothing to do
    if ( ! closestEdge ) return;

    // If the wall is further than our target, we can ignore it.
    if (this.distanceFromPathTarget() < closestDistance) return;
    var edge = closestEdge;
    var sign = -this.velocity.sign(edge.normal);
    var rotation = sign * (Math.PI/16);
    var p = 1-(closestDistance/100);
    //var rotation = sign * (Math.PI/2 * (100-(closestDistance/100)));
    var rotation = sign * (Math.PI/2 * p);
    this.velocity = this.velocity.rotate(rotation, origin);
};

AI.prototype.applyVelocity = function () {
    if ( !this.position || !this.velocity || !this.velocity.length() ) return;
    this.position = this.getFuturePosition();
    if (this.schedulePath && this.path) {
        this.pathNodes = this.path.replan(this.position);
        delete this.schedulePath;
    }
}

function getPathNodes(path, groupId) {
    var nodes = [];
    path.forEach(function (node, idx) {
        if (idx == 0) return;
        if (node.x == path[idx-1].x && node.y == path[idx-1].y) return;
        nodes.push($V([node.x, node.y]));
    });
    return nodes;
}

function sign(p1, p2, p3) {
    return (p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y);
}

function PointInTriangle (pt, v1, v2, v3) {
    var b1, b2, b3;

    b1 = sign(pt, v1, v2) < 0;
    b2 = sign(pt, v2, v3) < 0;
    b3 = sign(pt, v3, v1) < 0;

    return ((b1 == b2) && (b2 == b3));
}

*////////////

/*function futurePointOfCollision(x1, y1, x2, y2, cx, cy, cr) {
    var deltaY = (cy - y1);
    var deltaX = (cx - x1);
    var circleAngleInDeg = Math.atan2(deltaY, deltaX) * (180/Math.PI);

    var dx = x2 - x1
    var dy = y2 - y1
    var a = dx * dx + dy * dy
    var b = 2 * (dx * (x1 - cx) + dy * (y1 - cy))
    var c = cx * cx + cy * cy
    c = c + (x1 * x1 + y1 * y1)
    c = c - (2 * (cx * x1 + cy * y1))
    c = c - (cr * cr)
    local bb4ac = b * b - 4 * a * c

    if (bb4ac < 0) {
        return false; // No collision
    } else {
        return true; // Collision
    }
}*/



/*

function initPortals(path, startPos, endPos) {
    startPos = startPos || path[0].center;
    endPos = endPos || path[path.length-1].center;

    var portals = [];

    portals.push({
        left: startPos,
        right: startPos
    });

    path.forEach(function (triangle, idx) {
        if (idx == 0) return;
        
        var previousTriangle = path[idx-1];

        triangle.sides.forEach(side => {
            side.triangles.forEach(triangle => {
                if (triangle != previousTriangle) return;

                let corners = triangle.corners.slice(0);
                while (corners[0].index == side.points[0].index || corners[0].index == side.points[1].index) {
                    corners.push(corners.shift());
                }

                portals.push({
                    //left: points[pointIndices[2]],
                    //right: points[pointIndices[1]],
                    left: corners[2],
                    right: corners[1]
                });
            });
        });
    });

    portals.push({
        left: endPos,
        right: endPos
    });

    return portals;
}



function funnel(portals) {
    var pts = [];

    // Init scan state
    var portalApex, portalLeft, portalRight;
    var apexIndex = 0;
    var leftIndex = 0;
    var rightIndex = 0;

    portalApex  = portals[0].left;
    portalLeft  = portals[0].left;
    portalRight = portals[0].right;

    // Add start point.
    pts.push(portalApex);

    for (var i = 1; i < portals.length; i++) {
        var left = portals[i].left;
        var right = portals[i].right;

        // Update right vertex.
        if (triarea2(portalApex, portalRight, right) <= 0.0) {
            if (vequal(portalApex, portalRight) || triarea2(portalApex, portalLeft, right) > 0.0) {
                // Tighten the funnel.
                portalRight = right;
                rightIndex = i;
            } else {
                // Right over left, insert left to path and restart scan from portal left point.
                pts.push(portalLeft);
                // Make current left the new apex.
                portalApex = portalLeft;
                apexIndex = leftIndex;
                // Reset portal
                portalLeft = portalApex;
                portalRight = portalApex;
                leftIndex = apexIndex;
                rightIndex = apexIndex;
                // Restart scan
                i = apexIndex;
                continue;
            }
        }

        // Update left vertex.
        if (triarea2(portalApex, portalLeft, left) >= 0.0) {
            if (vequal(portalApex, portalLeft) || triarea2(portalApex, portalRight, left) < 0.0) {
                // Tighten the funnel.
                portalLeft = left;
                leftIndex = i;
            } else {
                // Left over right, insert right to path and restart scan from portal right point.
                pts.push(portalRight);
                // Make current right the new apex.
                portalApex = portalRight;
                apexIndex = rightIndex;
                // Reset portal
                portalLeft = portalApex;
                portalRight = portalApex;
                leftIndex = apexIndex;
                rightIndex = apexIndex;
                // Restart scan
                i = apexIndex;
                continue;
            }
        }
    }

    if ((pts.length === 0) || (!vequal(pts[pts.length - 1], portals[portals.length - 1].left))) {
        // Append last point to path.
        pts.push(portals[portals.length - 1].left);
    }

    //this.path = pts;
    return pts;
};

    function triarea2(a, b, c) {
        //var ax = b.x - a.x;
        //var az = b.z - a.z;
        //var bx = c.x - a.x;
        //var bz = c.z - a.z;
        var ax = b.x - a.x;
        var ay = b.y - a.y;
        var bx = c.x - a.x;
        var by = c.y - a.y;

        return bx * ay - ax * by;
    }

    function vequal(a, b) {
        return distanceToSquared(a, b) < 0.00001;
    }

    var distanceToSquared = function (a, b) {
        var dx = a.x - b.x;
        var dy = a.y - b.y;
        var dz = 0 - 0;

        return dx * dx + dy * dy + dz * dz;
    };
   
//function getPathNodes(path, groupId) {
function getPathNodes(path): Array<Vector2> {
    var nodes: Array<Vector2> = [];
    path.forEach(function (node, idx) {
        if (idx == 0) return;
        if (node.x == path[idx-1].x && node.y == path[idx-1].y) return;
        nodes.push(new Vector2(node.x, node.y));
    });
    return nodes;
}

*/
