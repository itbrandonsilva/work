import Vector2 from './Vector2.ts';
import * as _ from 'lodash';
import Renderer from './Renderer.ts';

/*interface Shape {
    x?: number;
    y?: number;
    processed?: boolean;
    index?: number;
    normal?: Vector2;
}*/

type IsSolidCallback = (value: any) => boolean;
interface Edge {
    processed?: boolean;
}

export class Tiles {
    private _isSolid: IsSolidCallback;
    tiles: Array<any>;
    width: number;
    height: number;

    constructor(width: number, isSolid: IsSolidCallback, tiles: Array<any>) {
        this.tiles = tiles;
        this.width = width;
        this.height = tiles.length/width;
        this._isSolid = isSolid;
    }

    getAt(x: number, y: number): number {
        if ( x < 0 || y < 0 ) return undefined;
        if ( x+1 > this.width || y+1 > this.height ) return undefined;
        return this.tiles[(y*this.width)+x];
    }

    isSolidAt(x: number, y: number) {
        let value = this.getAt(x, y);
        if ( value === undefined ) return true;
        let isSolid = this._isSolid(value);
        return isSolid;
    }
}

export class ShapeSide {
    points: Array<ShapeCorner>;
    normal: Vector2;
    index: number = null;

    constructor(v1: ShapeCorner, v2: ShapeCorner) {
        this.points = [v1, v2];
        this.normal = v2.clone().subtract(v1).normalize().rotate(-90);
    }
}

export class ShapeCorner extends Vector2 {
    sides: Array<ShapeSide> = [];
    normal: Vector2 = null;
    index: number = null;

    constructor(x, y) {
        super(x, y);
    }
}

export class Shape {
    points: Array<ShapeCorner> = [];
    sides: Array<ShapeSide> = [];
    scale: number;
    margin: number;
    scaledFrom: Shape;

    constructor(points: Array<Vector2>, scale: number = 1, offset: number = 0) {
        this.scale = scale;

        points.forEach((point, idx) => {
            let corner = new ShapeCorner(point.x, point.y);
            corner.index = idx;

            corner.x *= scale;
            corner.y *= scale;

            corner.x += offset;
            corner.y += offset;
            this.points.push(corner);
        });

        // These calls can be made optional, in the future. Selfish convenience, for now.
        this.simplify();
        this.generateSides();
        this.generateNormals();
    }

    // Reduce straight edges of multiple segments into single segments
    simplify() {
        let newPoints = [];

        this.points.forEach((corner, idx) => {
            if (newPoints.length === 0) newPoints.push(corner);
            else {
                let prevIdx = newPoints.length-1;
                let previousCorner  = newPoints[prevIdx];
                let previousCorner2 = newPoints[prevIdx-1];

                if (idx === this.points.length-1) {
                    let firstCorner = this.points[0];
                    if (corner.x === firstCorner.x && corner.y === firstCorner.y) {
                        if      (previousCorner2 && corner.x === previousCorner2.x) newPoints.pop();
                        else if (previousCorner2 && corner.y === previousCorner2.y) newPoints.pop();
                        return;
                    }
                }

                if      (corner.x === previousCorner.x && previousCorner2 && corner.x === previousCorner2.x) newPoints[prevIdx] = corner;
                else if (corner.y === previousCorner.y && previousCorner2 && corner.y === previousCorner2.y) newPoints[prevIdx] = corner;
                else    newPoints.push(corner);
            }
        });

        this.points = newPoints;
    }

    generateSides() {
        this.sides.length = 0;
        this.points.forEach(point => point.sides.length = 0);

        this.points.forEach((point, idx) => {
            let nextPoint = this.points[ (idx+1)%this.points.length ];
            let side = new ShapeSide(point, nextPoint);

            point.sides.push(side);
            nextPoint.sides.push(side);
            this.sides.push(side);
        });
    }

    generateNormals() {
        this.points.forEach(point => {
            let normal = new Vector2(0, 0);
            point.sides.forEach(side => normal.add(side.normal));
            normal.div(2).normalize();
            point.normal = normal;
        });

    }

    // Messy implementation for a specific need. Probably should pull this
    // method out and rebrand it as a util.
    inflateShape(margin: number): Shape {
        if (this.scaledFrom) throw new Error('This shape is a result of a previous expansion.');

        let expandedShape = new Shape(this.points.map(corner => {
            /*let cloned = _.cloneWith(corner, (value: ShapeCorner, key) => {
                if (key === 'normal') return value.clone();
                return value;
            });*/
            let cloned = _.clone(corner);
            cloned.normal = corner.normal.clone();

            let vector = new Vector2(cloned.x, cloned.y);
            vector.add(cloned.normal.mult(this.scale * margin).clone());

            cloned.x = vector.x;
            cloned.y = vector.y;

            return cloned;
        }));

        expandedShape.scale = this.scale;
        expandedShape.scaledFrom = this;
        expandedShape.margin = margin;

        return expandedShape;
    }
}

type CellIndex = {points: Array<ShapeCorner>, sides: Array<ShapeSide>};

export class ShapeIndex {
    index: Array<CellIndex> = [];
    width: number;
    height: number;
    sample: number;
    shape: Shape;

    constructor(shape: Shape, sample: number) {
        this.shape = shape;
        this.buildIndex(shape, sample);
    }

    rebuildIndex(sample: number) {
        this.buildIndex(this.shape, sample);
    }

    buildIndex(shape: Shape, sample: number) {
        this.index.length = 0;
        this.sample = sample;
        let width = 0;
        let height = 0;

        shape.points.forEach(point => {
            let x = Math.floor(point.x/sample);
            let y = Math.floor(point.y/sample);

            x+1 > width  ? width  = x+1 : null;
            y+1 > height ? height = y+1 : null;
        });

        this.width = width;
        this.height = height;

        let length = width * height;
        for (let l = 0; l < length; ++l) {
            this.index.push({points: [], sides: []});
        }

        shape.points.forEach(point => {
            let x = Math.floor(point.x/sample);
            let y = Math.floor(point.y/sample);
            this.index[this.getIndexOf(x, y)].points.push(point);
            //points.push({x, y, point});
        });  

        //shape.points.forEach(point => {
        //    this.index[this.getIndexOf(point.x, point.y)] = point.point;
        //});

        shape.sides.forEach(side => {
            this.castBetween(side.points[0], side.points[1], index => {
                index.sides.push(side);
                return false;
            });
        });
    }

    getIndexOf(x: number, y: number): number {
        return (this.width*y)+x;
    }

    getIndexAt(x: number, y: number): CellIndex {
        let index = this.index[this.getIndexOf(x, y)];
        return index;
    }

    /*
    _bresenham(v1: Vector2, v2: Vector2): Array<ShapeCorner> {
        console.log('bresenham()');
        let corners = [];

        if (v2.x < v1.x) [v1, v2] = [v2, v1];

        let dx  = Math.abs(v2.x - v1.x);
        let dy  = Math.abs(v2.y - v1.y);
        let sx  = (v1.x < v2.x) ? 1 : -1;
        let sy  = (v1.y < v2.y) ? 1 : -1;
        let err = dx - dy;

        let ox;
        let oy;

        //console.log(dx, dy, dx, sy, err);

        v1.log();
        v2.log();

        while (true) {
            this.result.push(v1.clone());

            corners = [...corners, ...this.getCornersAt(v1.x, v1.y)];

            if ( (ox !== undefined && oy !== undefined ) && (ox !== v1.x && oy !== v1.y) ) {
                corners = [...corners, this.getCornersAt(v1.x-1, v1.y)];
                //if ( (v1.y - oy) < 0 ) corners = [...corners, ...this.getCornersAt(v1.x, v1.y+1)];
                //else                   corners = [...corners, ...this.getCornersAt(v1.x, v1.y-1)];

                //console.log('Y:', oy, v1.y);
                //if ( (v1.y - oy) < 0 ) { this.result.push(v1.clone().add(new Vector2(-1, 0))); this.result.push(v1.clone().add(new Vector2(0, 1))); }
                //else                   { this.result.push(v1.clone().add(new Vector2(-1, 0))); this.result.push(v1.clone().add(new Vector2(0, -1))); }
            }

            if ( (v1.x == v2.x) && ( v1.y == v2.y ) ) break;

            ox = v1.x; oy = v1.y;

            var e2 = 2*err;
            if (e2 > -dy) { err -= dy; v1.x += sx; }
            if (e2 <  dx) { err += dx; v1.y += sy; }

            //v1.x += sx;
            //v1.y += sy;
        }

        return corners;
    }
    */

    /*
    _bresenham(v1: Vector2, v2: Vector2): Array<ShapeCorner> {
        let difX = v2.x - v1.x;
        let difY = v2.y - v1.y;
        let dist = Math.ceil(Math.abs(difX) + Math.abs(difY));

        let dx = difX / dist;
        let dy = difY / dist;

        for (let i = 0; i <= dist; i++) {
            let x = Math.floor(v1.x + dx * i);
            let y = Math.floor(v1.y + dy * i);
            //draw(x,y);
            this.result.push(new Vector2(x, y));
        }

        return [];
        //return true;
    }
    */

    // Frac if fractional part of float, for example, Frac(1.3) = 0.3, Frac(-1.7)=0.7
    frac0(f) {
        //return Math.abs(f % 1);
        return f % 1;
    }

    frac1(f) {
        return 1 - f + Math.floor(f);
    }

    /*clipVector(v: Vector2) {
        v.x = Math.max(0, v.x);
        v.x = Math.min(v.x, (this.width-1) * this.sample);

        v.y = Math.max(0, v.y);
        v.y = Math.min(v.y, (this.height-1) * this.sample);
    }*/

    castBetween(v1: Vector2, v2: Vector2, cb: (index: CellIndex, cell?: Vector2) => boolean ): void {
        v1 = v1.clone();
        v2 = v2.clone();

        // Handle points directly on an edge
        let direction = v2.clone().subtract(v1);
        if (v2.x % this.sample === 0) {
            v2.x = v2.x + (-Math.sign(direction.x)*0.001);
        }

        if (v2.y % this.sample === 0) {
            v2.y = v2.y + (-Math.sign(direction.y)*0.001);
        }

        v1 = v1.div(this.sample);
        v2 = v2.div(this.sample);

        CohenSutherlandLineClipAndDraw(v1, v2, this.width-0.01, this.height-0.01);
        
        /*
        let dx = v2.x - v1.x;
        let tDeltaX = 1 / dx;
        let tMaxX = tDeltaX * (1 - this.frac(v1.x));

        let dy = v2.y - v1.y;
        let tDeltaY = 1 / dy;
        let tMaxY = tDeltaY * (1 - this.frac(v1.y));
        */

        let tDeltaX, tMaxX;
        let dx = Math.sign(v2.x - v1.x);
        if ( dx !== 0 ) tDeltaX = Math.min(dx / (v2.x - v1.x), 10000000);
        else tDeltaX = 10000000;
        if ( dx > 0 ) tMaxX = tDeltaX * this.frac1(v1.x);
        else tMaxX = tDeltaX * this.frac0(v1.x);

        let tDeltaY, tMaxY;
        let dy = Math.sign(v2.y - v1.y);
        if ( dy !== 0 ) tDeltaY = Math.min(dy / (v2.y - v1.y), 10000000);
        else tDeltaY = 10000000;
        if ( dy > 0 ) tMaxY = tDeltaY * this.frac1(v1.y);
        else tMaxY = tDeltaY * this.frac0(v1.y);


        let x = v1.x;
        let y = v1.y;
        let stepX = dx > 0 ? 1 : -1;
        let stepY = dy > 0 ? 1 : -1;

        let destX = Math.floor(v2.x);
        let destY = Math.floor(v2.y);

        /*
        console.log('------------------');
        console.log(v1.x, v1.y);
        console.log(v2.x, v2.y);
        console.log(dx, dy);
        console.log('');
        console.log(x, y);
        console.log(destX, destY);
        console.log('Delta:', tDeltaX, tDeltaY);
        console.log('tMax:', tMaxX, tMaxY);
        */

        while (true) {
            let fx = Math.floor(x);
            let fy = Math.floor(y);

            if (fx+1 > this.width || fy+1 > this.height) { console.log('B1'); break; }
            if (fx < 0 || fy < 0) { console.log('B2'); break; }

            let index = this.getIndexAt(fx, fy);
            if ( ! index ) console.log('!?!?!?', this.width, this.height, fx, fy);

            let ctx = Renderer.ctx();
            ctx.beginPath();
            ctx.rect(fx*this.sample, fy*this.sample, this.sample, this.sample);
            ctx.strokeStyle = 'purple';
            ctx.lineWidth = 6;
            ctx.stroke();

            if ( cb(index, new Vector2(fx, fy)) ) break;
            //if (tMaxX > 1 || tMaxY > 1) break;

            if (destX === fx && destY === fy) break;

            if (tMaxX < tMaxY)
            {
                tMaxX = tMaxX + (tDeltaX);
                x = x + stepX;
            }
            else
            {
                tMaxY = tMaxY + (tDeltaY);
                y = y + stepY;
            }
        }

            let ctx = Renderer.ctx();
            ctx.beginPath();
            ctx.rect(destX*this.sample, destY*this.sample, this.sample, this.sample);
            ctx.strokeStyle = 'orange';
            ctx.lineWidth = 6;
            ctx.stroke();

    }

    /*
    getIndicesBetween(v1: Vector2, v2: Vector2): Array<ShapeCorner> {
        //let startX = v1.x / this.sample;
        //let startY = v1.y / this.sample;

        //let endX = v2.x / this.sample;
        //let endY = v2.y / this.sample;

        //let corners = this._bresenham(new Vector2(startX, startY), new Vector2(endX, endY));
        //let corners = this._bresenham(v1, v2);
        
        let corners = [];
        return corners;
    }
    */
}

export const extractShapesFromTiles = function(tiles: Tiles, scale: number = 1, offset: number = 0): Array<Shape> {
    let hEdges: Array<Array<Edge>> = [];
    for (let i = 0; i < tiles.height+1; ++i) {
        let a: Array<any> = [];
        //a.length = width;
        hEdges.push(a);
    }
    let vEdges: Array<Array<Edge>> = [];
    for (let i = 0; i < tiles.height; ++i) {
        let a: Array<any> = [];
        //a.length = width;
        vEdges.push(a);
    }

    for (let y = 0; y < tiles.height; ++y) {
        for (let x = 0; x < tiles.width; ++x) {
            let isSolid = tiles.isSolidAt(x, y);
            if ( isSolid ) continue;

            // top
            if ( tiles.isSolidAt(x, y-1) ) hEdges[y][x] = {};
            // bottom
            if ( tiles.isSolidAt(x, y+1) ) hEdges[y+1][x] = {};
            // left
            if ( tiles.getAt(x-1, 0) === undefined || tiles.isSolidAt(x-1, y) ) vEdges[y][x] = {};
            // right 
            if ( !tiles.getAt(x+1, 0) === undefined || tiles.isSolidAt(x+1, y) ) vEdges[y][x+1] = {};
        }
    }

    function trace(corners: Array<Vector2>): void {
        let p = corners[corners.length-1];
        // right
        if (p.x < tiles.width) {
            let edge = hEdges[p.y][p.x];
            if (edge && !edge.processed) {
                corners.push(new Vector2(p.x+1, p.y));
                edge.processed = true;
                return trace(corners);
            }
        }
        // down
        if (p.y < tiles.height) {
            let edge = vEdges[p.y][p.x];
            if (edge && !edge.processed) {
                corners.push(new Vector2(p.x, p.y+1));
                edge.processed = true;
                return trace(corners);
            }
        }
        // left
        if (p.x > 0) {
            let edge = hEdges[p.y][p.x-1];
            if (edge && !edge.processed) {
                corners.push(new Vector2(p.x-1, p.y));
                edge.processed = true;
                return trace(corners);
            }
        }
        // up
        if (p.y > 0) {
            let edge = vEdges[p.y-1][p.x];
            if (edge && !edge.processed) {
                corners.push(new Vector2(p.x, p.y-1));
                edge.processed = true;
                return trace(corners);
            }
        }
    };

    /*
    let tl = {v: new Vector2(-1, -1), n: new Vector2(-1, -1)};
    let tr = {v: new Vector2( 0, -1), n: new Vector2( 1, -1)};
    let bl = {v: new Vector2(-1,  0), n: new Vector2(-1,  1)};
    let br = {v: new Vector2( 0,  0), n: new Vector2( 1,  1)};
    let cells = [tl, tr, bl, br];
    function calcNormal(point: Vector2) {
        let solids = cells.filter(cell => { return tiles.isSolidAt(point.x + cell.v.x, point.y + cell.v.y)});
        return solids.reduce((prev, curr) => {
            return prev.add(curr.n);
        }, new Vector2(0, 0)).normalize();
    }
    */

    let shapes: Array<Shape> = [];
    for (let y = 0; y < tiles.height-1; ++y) {
        for (let x = 0; x < tiles.width; ++x) {
            let vEdge = vEdges[y][x];
            if (vEdge && !vEdge.processed) {
                let corners: Array<Vector2> = [];
                corners.push(new Vector2(x, y));
                trace(corners);
                //let shapeCorners: Array<ShapeCorner> = corners.map(corner => {
                    //return new ShapeCorner(corner.x, corner.y, calcNormal(corner));
                //    return new ShapeCorner(corner.x, corner.y);
                //});
                let shape = new Shape(corners, scale, offset);
                shapes.push(shape);
            }
        }
    }

    return shapes;
}


export const drawShape = function (shape: Shape, color: string = 'black') {
    var canvas = <HTMLCanvasElement> document.getElementById('view');
    var ctx = canvas.getContext('2d');

    ctx.strokeStyle = color;
    ctx.lineWidth = 1;
    ctx.beginPath();
    shape.points.forEach((point, idx) => {
        if (idx === 0) return ctx.moveTo(point.x, point.y);
        ctx.lineTo(point.x, point.y);

        if (idx === shape.points.length-1) ctx.lineTo(shape.points[0].x, shape.points[0].y);
    });

    ctx.stroke();
}


const INSIDE = 0; // 0000
const LEFT = 1;   // 0001
const RIGHT = 2;  // 0010
const BOTTOM = 4; // 0100
const TOP = 8;    // 1000

// Compute the bit code for a point (x, y) using the clip rectangle
// bounded diagonally by (xmin, ymin), and (xmax, ymax)

// ASSUME THAT xmax, xmin, ymax and ymin are global constants.

const ComputeOutCode = function(v: Vector2, width: number, height: number): number {
    let code = INSIDE;       // initialised as being inside of [[clip window]]

    if (v.x < 0)             // to the left of clip window
        code |= LEFT;
    else if (v.x > width)    // to the right of clip window
        code |= RIGHT;
    if (v.y < 0)             // below the clip window
        code |= BOTTOM;
    else if (v.y > height)   // above the clip window
        code |= TOP;

    return code;
}

// Cohen–Sutherland clipping algorithm clips a line from
// P0 = (x0, y0) to P1 = (x1, y1) against a rectangle with 
// diagonal from (xmin, ymin) to (xmax, ymax).
const CohenSutherlandLineClipAndDraw = function(v1: Vector2, v2: Vector2, width: number, height: number): void {
    // compute outcodes for P0, P1, and whatever point lies outside the clip rectangle
    let outcode0: number = ComputeOutCode(v1, width, height);
    let outcode1: number = ComputeOutCode(v2, width, height);
    let accept = false;

    while (true) {
        if (! (outcode0 | outcode1)) { // Bitwise OR is 0. Trivially accept and get out of loop
            accept = true;
            break;
        } else if (outcode0 & outcode1) { // Bitwise AND is not 0. Trivially reject and get out of loop
            break;
        } else {
            // failed both tests, so calculate the line segment to clip
            // from an outside point to an intersection with clip edge
            let x: number;
            let y: number;

            // At least one endpoint is outside the clip rectangle; pick it.
            let outcodeOut: number = outcode0 ? outcode0 : outcode1;

            // Now find the intersection point;
            // use formulas y = y0 + slope * (x - x0), x = x0 + (1 / slope) * (y - y0)
            if (outcodeOut & TOP) {           // point is above the clip rectangle
                x = v1.x + (v2.x - v1.x) * (height - v1.y) / (v2.y - v1.y);
                y = height;
            } else if (outcodeOut & BOTTOM) { // point is below the clip rectangle
                x = v1.x + (v2.x - v1.x) * (0 - v1.y) / (v2.y - v1.y)//                     x0 + (x1 - x0) * (ymin - y0) / (y1 - y0);
                y = 0;
            } else if (outcodeOut & RIGHT) {  // point is to the right of clip rectangle
                x = width;
                y = v1.y + (v2.y - v1.y) * (width - v1.x) / (v2.x - v1.x);//                                      y0 + (y1 - y0) * (xmax - x0) / (x1 - x0);
            } else if (outcodeOut & LEFT) {   // point is to the left of clip rectangle
                x = 0;
                y = v1.y + (v2.y - v1.y) * (0 - v1.x) / (v2.x - v1.x);
            }

            // Now we move outside point to intersection point to clip
            // and get ready for next pass.
            if (outcodeOut == outcode0) {
                v1.x = x;
                v1.y = y;
                outcode0 = ComputeOutCode(v1, width, height);
            } else {
                v2.x = x;
                v2.y = y;
                outcode1 = ComputeOutCode(v2, width, height);
            }
        }
    }
    //if (accept) {
               // Following functions are left for implementation by user based on
               // their platform (OpenGL/graphics.h etc.)
               //DrawRectangle(xmin, ymin, xmax, ymax);
               //LineSegment(x0, y0, x1, y1);
    //}
}
