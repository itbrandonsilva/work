import Vector2 from './Vector2.ts';
import * as _ from 'lodash';

interface Shape {
    x?: number;
    y?: number;
    processed?: boolean;
    index?: number;
    normal?: Vector2;
}

const extractBodies = function (tilesInput: Array<number>, width: number, solids: Array<number>, scale: number = 1, margin: number = 0.3) {
    let height: number = tilesInput.length/width;

    function isSolid(x, y) {
        if ( x < 0 || y < 0 ) return true;
        console.log('-------------');
        let value: any = tilesInput[(y*width)+x];

        console.log(value);
        value = (value === 0 || value === undefined ? true : false);
        console.log(x, y, value);
        return value;
    }

    let tiles: Array<Array<number>> = [];
    for (let x = 0; x < width; ++x) {
        let column: Array<number> = [];
        tiles.push(column);
        for (let y = 0; y < height; ++y) {
            column[y] = tilesInput[(y*width)+x];
        }
    }

    //let width = tiles.length;
    //let height = tiles[0].length;

    let hEdges: Array<Array<Shape>> = [];
    for (let i = 0; i < height+1; ++i) {
        let a: Array<Shape> = [];
        //a.length = width;
        hEdges.push(a);
    }
    let vEdges: Array<Array<Shape>> = [];
    for (let i = 0; i < height; ++i) {
        let a: Array<Shape> = [];
        //a.length = width;
        vEdges.push(a);
    }

    for (let y = 0; y < height; ++y) {
        for (let x = 0; x < width; ++x) {
            let tile = tiles[x][y];
            if (tile < 1) continue;
            // top
            if ( (tiles[x][y-1] || 0) < 1) hEdges[y][x] = {};
            // bottom
            if ( (tiles[x][y+1] || 0) < 1) hEdges[y+1][x] = {};
            // left
            if ( !tiles[x-1] || ((tiles[x-1][y] || 0) < 1)) vEdges[y][x] = {};
            // right 
            if ( !tiles[x+1] || ((tiles[x+1][y] || 0) < 1)) vEdges[y][x+1] = {};
        }
    }

    function seek(shape: Array<Shape>): void {
        let p = shape[shape.length-1];
        // right
        if (p.x < width) {
            let edge = hEdges[p.y][p.x];
            if (edge && !edge.processed) {
                shape.push({x: p.x+1, y: p.y});
                edge.processed = true;
                return seek(shape);
            }
        }
        // down
        if (p.y < height) {
            let edge = vEdges[p.y][p.x];
            if (edge && !edge.processed) {
                shape.push({x: p.x, y: p.y+1});
                edge.processed = true;
                return seek(shape);
            }
        }
        // left
        if (p.x > 0) {
            let edge = hEdges[p.y][p.x-1];
            if (edge && !edge.processed) {
                shape.push({x: p.x-1, y: p.y});
                edge.processed = true;
                return seek(shape);
            }
        }
        // up
        if (p.y > 0) {
            let edge = vEdges[p.y-1][p.x];
            if (edge && !edge.processed) {
                shape.push({x: p.x, y: p.y-1});
                edge.processed = true;
                return seek(shape);
            }
        }
    };

    let shapes: Array<Array<Shape>> = [];
    for (let y = 0; y < height-1; ++y) {
        for (let x = 0; x < width; ++x) {
            let vEdge = vEdges[y][x];
            if (vEdge && !vEdge.processed) {
                let shape = [
                    //{x: x, y: y+1},
                    {x: x, y: y},
                ];
                seek(shape);
                shapes.push(shape);
            }
        }
    }

    let tl = {v: new Vector2(-1, -1), n: new Vector2(-1, -1)};
    let tr = {v: new Vector2( 0, -1), n: new Vector2( 1, -1)};
    let bl = {v: new Vector2(-1,  0), n: new Vector2(-1,  1)};
    let br = {v: new Vector2( 0,  0), n: new Vector2( 1,  1)};
    let cells = [tl, tr, bl, br];
    function calcNormal(point: Shape) {
        let solids = cells.filter(cell => { return isSolid(point.x + cell.v.x, point.y + cell.v.y)});
        point.normal = solids.reduce((prev, curr) => {
            return prev.add(curr.n);
        }, new Vector2(0, 0));
        point.normal.normalize();
    }

    // Reduce straight edges of multiple segments into single segments
    let refinedShapes: Array<Array<Shape>> = [];
    shapes.forEach((s, idx) => {
        let refinedShape = [];
        refinedShapes.push(refinedShape);
        s.forEach((shape, idx) => {
            if ( ! refinedShape.length ) return refinedShape.push(shape);
            let previous =  refinedShape[refinedShape.length-1];
            let previous2 = refinedShape[refinedShape.length-2];
            if (previous.x === shape.x) {
                if (previous2 && previous2.x === shape.x) {
                    refinedShape[refinedShape.length-1] = shape;
                    return;
                }
            }
            else if (previous.y === shape.y) {
                if (previous2 && previous2.y === shape.y) {
                    refinedShape[refinedShape.length-1] = shape;
                    return;
                }
            }
            refinedShape.push(shape);
        });


        refinedShape.forEach(point => {
            calcNormal(point);
        });
    });

    console.log(JSON.stringify(refinedShapes));

    //shapes.forEach(function (shape) {
    refinedShapes.forEach(shape => {
        shape.forEach(function (point, idx) {
            point.x*=scale; point.y*=scale;
            point.index = idx;
        });
    });

    return refinedShapes;
}

export default extractBodies;

export const drawShape = function (shape) {
    var canvas = document.getElementById('view');
    var ctx = canvas.getContext('2d');

    ctx.strokeStyle = 'black';
    ctx.lineWidth = 1;
    ctx.beginPath();
    shape.forEach((point, idx) => {
        if (idx == 0) return ctx.moveTo(point.x, point.y);
        ctx.lineTo(point.x, point.y);
    });

    ctx.stroke();
}

export const expandShape = function (shape, scale, margin) {
    return shape.map(point => {
        let cloned = _.cloneWith(point, (value, key) => {
            if (key === 'normal') return value.clone();
            return value;
        });

        let vector = new Vector2(cloned.x, cloned.y);
        vector.add(cloned.normal.mult(scale * margin).clone());

        cloned.x = vector.x;
        cloned.y = vector.y;

        return cloned;
    });
}
