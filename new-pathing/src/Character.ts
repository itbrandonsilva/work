import Vector2 from './Vector2';
import { PointPath, PointPathNode } from './NavMesh';
//import { Agent } from './AISystem.ts';
import World from './World';
import Renderer from './Renderer';

/*
export class Agent {

    constructor(position?: Vector2, radius: number = 8, id?: number) {
        this.position = position || new Vector2();
        this.velocity = new Vector2();
        this.radius = radius;
    }   

    draw() {
        var canvas = <HTMLCanvasElement> document.getElementById('view');
        var ctx = canvas.getContext('2d');

        ctx.fillStyle = "red";
        ctx.beginPath();
        ctx.arc(this.position.x, this.position.y, this.radius, 0, Math.PI*2);
        ctx.closePath();
        ctx.fill();

        if (this.path) this.path.draw();
    }

}
*/


export default class Character {
    position: Vector2;
    velocity: Vector2;
    radius: number;
    private path: PointPath;
    world: World = null;
    seek: Vector2 = null;
    nextNode: PointPathNode = null;
    speed: number = 80;
    id: number = null;

    constructor(position: Vector2, radius: number = 8, id?: number) {
        this.position = position || new Vector2();
        this.velocity = new Vector2();
        this.radius = radius;
    }

    step() {
        this.position.add(this.velocity);
    }   

    setWorld(world: World) {
        this.world = world;
    }

    updateNextNode() {
        let path = this.getPath();
        if ( ! path ) return;
        if ( ! this.nextNode ) this.nextNode = path.nodes[1];

        //console.log('nex:', nextIndex);
        //console.log(path);

        let nextIndex = path.nodes.indexOf(this.nextNode)+1;
        let nodeAfterNext = path.nodes[nextIndex];

        if ( ! nodeAfterNext ) return;

        /*
        let ctx = Renderer.ctx();
        ctx.beginPath();
        ctx.strokeStyle = 'purple';
        ctx.lineWidth = 2;
        ctx.moveTo(this.position.x, this.position.y);
        ctx.lineTo(nodeAfterNext.x, nodeAfterNext.y);
        ctx.stroke();
        */

        this.world.physics.castBetween(this.position, nodeAfterNext, collision => {
            if ( ! collision ) {
                this.nextNode = nodeAfterNext;
            } else {

                /*
                collision.log();
                ctx.beginPath();
                ctx.fillStyle = 'orange';
                ctx.arc(collision.x, collision.y, 20, 0, Math.PI*2);
                ctx.fill();
                */

                this.world.physics.castBetween(this.position, this.nextNode, collision => {
                    if ( collision ) {
                        //console.log('updateNextNode() resolved to no path!');
                        console.log('Recalc...');
                        this.findPath(path.nodes[path.nodes.length-1]);
                    }
                    return true;
                });
            }
            return true;
        });
    }

    followPath() {
        this.updateNextNode();
        let path = this.getPath();
        if ( ! path ) return;

        let lastNode = path.nodes[path.nodes.length-1];
        let distance = lastNode.distance(this.position);
        if ( distance < 2 ) {
            this.setPath(null);
            this.nextNode = null;
            this.velocity.set(0, 0);
            return;
        }

        this.velocity.mimic(this.nextNode.clone().subtract(this.position)).setLength(this.speed);

        //this.velocity.log();
    }

    findPath(destination: Vector2): boolean {
        let path: PointPath = this.world.navigation.findPath(this.position, destination);
        if ( ! path ) {
            console.log('Character.findPath() - Failed to find path.');
            return false;
        }
        this.setPath(path);
        return true;
    }

    getPath(): PointPath {
        return this.path;
    }   

    setPath(path: PointPath): void {
        this.path = path;
    }   

    resetVelocity(): void {
        this.velocity.set(0, 0); 
    }   

    steer(ms: number): void {
        this.resetVelocity();
        this.followPath();
    }   

    applyVelocity(ms: number): void {
        let velocity = this.velocity.clone().mult(ms/1000)
        //velocity.log();
        this.position.add(velocity);
    }
}
