import Vector2 from './Vector2';
import Shape from './Shape';
import * as _ from 'lodash';
import Renderer from './Renderer';
import MathUtils from './MathUtils';
import Tiles from './Tiles';

interface Edge {
    processed?: boolean;
}

export default class ShapeUtils {
    static extractShapesFromTiles(tiles: Tiles, scale: number = 1, offset: number = 0): Array<Shape> {
        let hEdges: Array<Array<Edge>> = [];
        for (let i = 0; i < tiles.height+1; ++i) {
            let a: Array<any> = [];
            //a.length = width;
            hEdges.push(a);
        }
        let vEdges: Array<Array<Edge>> = [];
        for (let i = 0; i < tiles.height; ++i) {
            let a: Array<any> = [];
            //a.length = width;
            vEdges.push(a);
        }

        for (let y = 0; y < tiles.height; ++y) {
            for (let x = 0; x < tiles.width; ++x) {
                let isSolid = tiles.isSolidAt(x, y);
                if ( isSolid ) continue;

                // top
                if ( tiles.isSolidAt(x, y-1) ) hEdges[y][x] = {};
                // bottom
                if ( tiles.isSolidAt(x, y+1) ) hEdges[y+1][x] = {};
                // left
                if ( tiles.getAt(x-1, 0) === undefined || tiles.isSolidAt(x-1, y) ) vEdges[y][x] = {};
                // right 
                if ( !tiles.getAt(x+1, 0) === undefined || tiles.isSolidAt(x+1, y) ) vEdges[y][x+1] = {};
            }
        }

        function trace(corners: Array<Vector2>): void {
            let p = corners[corners.length-1];
            // right
            if (p.x < tiles.width) {
                let edge = hEdges[p.y][p.x];
                if (edge && !edge.processed) {
                    corners.push(new Vector2(p.x+1, p.y));
                    edge.processed = true;
                    return trace(corners);
                }
            }
            // down
            if (p.y < tiles.height) {
                let edge = vEdges[p.y][p.x];
                if (edge && !edge.processed) {
                    corners.push(new Vector2(p.x, p.y+1));
                    edge.processed = true;
                    return trace(corners);
                }
            }
            // left
            if (p.x > 0) {
                let edge = hEdges[p.y][p.x-1];
                if (edge && !edge.processed) {
                    corners.push(new Vector2(p.x-1, p.y));
                    edge.processed = true;
                    return trace(corners);
                }
            }
            // up
            if (p.y > 0) {
                let edge = vEdges[p.y-1][p.x];
                if (edge && !edge.processed) {
                    corners.push(new Vector2(p.x, p.y-1));
                    edge.processed = true;
                    return trace(corners);
                }
            }
        };

        /*
        let tl = {v: new Vector2(-1, -1), n: new Vector2(-1, -1)};
        let tr = {v: new Vector2( 0, -1), n: new Vector2( 1, -1)};
        let bl = {v: new Vector2(-1,  0), n: new Vector2(-1,  1)};
        let br = {v: new Vector2( 0,  0), n: new Vector2( 1,  1)};
        let cells = [tl, tr, bl, br];
        function calcNormal(point: Vector2) {
            let solids = cells.filter(cell => { return tiles.isSolidAt(point.x + cell.v.x, point.y + cell.v.y)});
            return solids.reduce((prev, curr) => {
                return prev.add(curr.n);
            }, new Vector2(0, 0)).normalize();
        }
        */

        let shapes: Array<Shape> = [];
        for (let y = 0; y < tiles.height-1; ++y) {
            for (let x = 0; x < tiles.width; ++x) {
                let vEdge = vEdges[y][x];
                if (vEdge && !vEdge.processed) {
                    let corners: Array<Vector2> = [];
                    corners.push(new Vector2(x, y));
                    trace(corners);
                    //let shapeCorners: Array<ShapeCorner> = corners.map(corner => {
                        //return new ShapeCorner(corner.x, corner.y, calcNormal(corner));
                    //    return new ShapeCorner(corner.x, corner.y);
                    //});
                    let shape = new Shape(corners, scale, offset);
                    shapes.push(shape);
                }
            }
        }

        return shapes;
    }
}
