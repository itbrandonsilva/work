import Vector2 from './Vector2';
import Character from './Character';
//import { Tiles, Shape, extractShapesFromTiles } from './tiler.ts';
import Game from './Game';
import World from './World';
import Renderer from './Renderer';

/*
let map = [0, 0, 1, 0, 1, 1, 1, 0, 0,
           0, 0, 1, 1, 1, 1, 0, 0, 0,
           0, 0, 0, 1, 0, 0, 0, 1, 1,
           0, 0, 0, 1, 0, 0, 0, 1, 1,
           0, 1, 1, 1, 1, 1, 0, 0, 1,
           0, 0, 0, 1, 1, 1, 1, 1, 1,
           1, 1, 1, 1, 0, 0, 0, 0, 1];
*/

/*
let map = [0, 0, 0, 0, 0,
           0, 1, 1, 1, 0,
           0, 1, 1, 1, 0,
           0, 1, 1, 1, 0,
           0, 0, 0, 0, 0];
*/

const TILE_W = 11;
//const TILE_H = 9;
const TILE_SCALE = 50;
var map = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
           0, 0, 1, 1, 0, 1, 1, 1, 0, 0, 0,
           0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0,
           0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0,
           0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 0,
           0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 0,
           0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0,
           0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0,
           0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
/*
var map = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
           0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0,
           0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0,
           0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 0,
           0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 0,
           0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 0,
           0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0,
           0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0,
           0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
*/

//let tiles = new Tiles(TILE_W, value => { return ( value < 1 ? true : false ) }, map);

//let shapes = Shape.fromTiles(tiles, TILE_SCALE, TILE_SCALE/2);
//let shapes = extractShapesFromTiles(tiles, TILE_SCALE, TILE_SCALE/2);

//let navmesh = new NavMesh(shapes[0]);
//let system = new AISystem(navmesh);

const isSolid = value => { return ( value < 1 ? true : false ) };
let world = new World(map, TILE_W, TILE_SCALE, isSolid);
let game = new Game(world);
//let tiles = new Tiles(TILE_W, value => { return ( value < 1 ? true : false ) }, map);

let canvas = Renderer.canvas();
let ctx = Renderer.ctx();

const draw = function(elapsed: number) {


/*
        //let v1 = new Vector2(106.9, 82.7);
        //let v2 = new Vector2(273.1, 569.2);

        //let v1 = new Vector2(36.9, 22.7);
        //let v2 = new Vector2(532.1, 379.2);

        let v1 = new Vector2(86.9, 218.2);
        let v2 = new Vector2(545, 392.7);

        //let v1 = new Vector2(55, 306);
        //let v2 = new Vector2(245, 135);

        //let v1 = new Vector2(463, 334);
        //let v2 = new Vector2(65, 110);


        //[v1, v2].forEach(v => index.clipVector(v));

        let indices: Array<any> = []; 
        world.physics.walls.castBetween(v1, v2, (index, cell) => {
            indices.push({index, cell});
            return false;
        }); 

        let sample = world.physics.walls.sample;

        indices.forEach(index => {
            ctx.beginPath();
            ctx.fillStyle = 'teal';
            ctx.rect(index.cell.x * sample, index.cell.y * sample, sample, sample);
            ctx.fill();
        });

        let sides = []; 
        indices.forEach(index => {
            sides = [...sides, ...index.index.sides];
        }); 
        sides = Array.from(new Set(sides));

        sides.forEach(side => {
            ctx.lineWidth = 6;
            ctx.strokeStyle = 'yellow';
            ctx.beginPath();
            ctx.moveTo(side.points[0].x, side.points[0].y);
            ctx.lineTo(side.points[1].x, side.points[1].y);
            ctx.stroke();
        }); 

        ctx.strokeStyle = 'black';
        ctx.lineWidth = 2;
        ctx.beginPath();
        ctx.moveTo(v1.x, v1.y);
        ctx.lineTo(v2.x, v2.y);
        ctx.stroke();
*/



        Renderer.drawShapeIndexGrid(world.physics.walls);
        Renderer.drawShape(world.physics.shape);
        //Renderer.drawNavMesh(world.navigation);
        Renderer.drawCharacters(game.characters);
        //if (character.getPath()) Renderer.drawPath(character.getPath(), 'green', 3);

}





//let character = new Character(new Vector2(4 * TILE_SCALE, 3 * TILE_SCALE));
//let character = new Character(new Vector2(8.5 * TILE_SCALE, 3.5 * TILE_SCALE));
let character = new Character(new Vector2(7.5 * TILE_SCALE, 2.5 * TILE_SCALE));
game.addCharacter(character);

//system.addAgent(character);
//system.selectAll();
//system.assignPathToSelection(new Vector2(9.5 * TILE_SCALE, 5 * TILE_SCALE));

//system.draw();

let last = 0;
const step = function(ms) {
    let elapsed = ms - last;
    last = ms;

        ctx.beginPath();
        ctx.fillStyle = 'white';
        ctx.rect(0, 0, canvas.width, canvas.height);
        ctx.fill();


    game.step(elapsed);
    draw(elapsed);
    window.requestAnimationFrame(step);
}

window.requestAnimationFrame(step);

window.onmousedown = function (e) {
    e.preventDefault();

    let x = e.clientX;
    let y = e.clientY;

    let button = e.button;
    switch (e.button) {
        case 0:
            console.log('setPosition()');
            character.position.x = x;
            character.position.y = y;
            break;
        case 2:
            //game.assignPath(character, new Vector2(8.5 * TILE_SCALE, 5 * TILE_SCALE));
            console.log('assignPath()');
            let vector = new Vector2(x, y);
            console.log(vector.x, vector.y);
            game.assignPath(character, vector);
            break;
    }
}

window.oncontextmenu = function (e) {
    e.preventDefault();
}

let p = 0;
function stepm() {
    p += 33;
    step(p);
}

//game.assignPath(character, new Vector2(110, 293));
/*for (let i = 0; i < 38; ++i) {
    stepm();
}*/

window.onkeydown = function (e) {
    e.preventDefault();
    // Space
    if (e.keyCode === 32) {
        //stepm();
    }
    console.log(e);
}
