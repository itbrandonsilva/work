import NavMesh, { PointPath } from './NavMesh';
import Character from './Character';
import Shape, { ShapeIndex } from './Shape';

export const canvas = <HTMLCanvasElement> document.getElementById('view');
export const ctx = canvas.getContext('2d');


//export const drawNavMesh(navmesh: NavMesh) {

//}

export default class Renderer {
    static canvas() {
        return canvas;
    }

    static ctx() {
        return ctx;
    }

    static drawShape(shape: Shape, color: string = 'red', lineWidth: number = 1): void {
        ctx.strokeStyle = color;
        ctx.lineWidth = lineWidth;

        ctx.beginPath();
        shape.points.forEach((point, idx) => {
            if (idx === 0) return ctx.moveTo(point.x, point.y);
            ctx.lineTo(point.x, point.y);

            if (idx === shape.points.length-1) ctx.lineTo(shape.points[0].x, shape.points[0].y);
        }); 
        ctx.stroke();
    }

    static drawNavMesh(navmesh: NavMesh, color: string = 'black', lineWidth: number = 1): void {
        ctx.lineWidth = lineWidth;

        for (let i = 0; i < navmesh.triangles.length; ++i) {
            let triangle = navmesh.triangles[i];

            ctx.beginPath();
            ctx.strokeStyle = color;

            ctx.moveTo(triangle.corners[0].x, triangle.corners[0].y);
            ctx.lineTo(triangle.corners[1].x, triangle.corners[1].y);
            ctx.lineTo(triangle.corners[2].x, triangle.corners[2].y);
            ctx.lineTo(triangle.corners[0].x, triangle.corners[0].y);
            ctx.stroke();

            ctx.fillStyle = 'black';
            ctx.font = '10px serif';
            ctx.fillText(triangle.index.toString(), triangle.center.x-3, triangle.center.y);
        } 
    }

    static drawCharacters(characters: Array<Character>, color: string = 'red'): void {
        characters.forEach(character => {
            ctx.fillStyle = color;
            ctx.beginPath();
            ctx.arc(character.position.x, character.position.y, character.radius, 0, Math.PI*2);
            ctx.closePath();
            ctx.fill();
        });

        //if (this.path) this.path.draw();
    }

    static drawPath(path: PointPath, color: string = 'green', lineWidth: number = 1): void {
        path.nodes.forEach((node, idx) => {
            if (idx+1 == path.nodes.length) return;
            let nextNode = path.nodes[idx+1];

            ctx.strokeStyle = color;
            ctx.lineWidth = lineWidth;
            ctx.beginPath();
            ctx.moveTo(node.x, node.y);
            ctx.lineTo(nextNode.x, nextNode.y);
            ctx.stroke();
        });
    }

    static drawShapeIndexGrid(index: ShapeIndex, color: string = 'teal', lineWidth: number = 1) {
        let sample = index.sample;
        for (let x = 0; x < index.width; ++x) {
            for (let y = 0; y < index.height; ++y) {
                ctx.beginPath();
                ctx.strokeStyle = color;
                ctx.lineWidth = lineWidth;
                ctx.rect(x * sample, y * sample, sample, sample);
                ctx.stroke();
            }
        }
    }
}
