import PhysicsWorld from './PhysicsWorld';
import Tiles from './Tiles';
import NavMesh from './NavMesh';
import ShapeUtils from './ShapeUtils';

export default class World {
    physics: PhysicsWorld;
    navigation: NavMesh;

    constructor(map: Array<number>, width: number, scale, isSolid: (value: number) => boolean) {
        console.log('Original scale:', scale);
        let tiles = new Tiles(width, isSolid, map);
        let shapes = ShapeUtils.extractShapesFromTiles(tiles, scale, 0);

        this.navigation = new NavMesh(shapes[0]);
        //let system = new AISystem(navmesh);

        // Physics walls
        let inflatedShape = shapes[0].inflateShape(0.3);
        this.physics = new PhysicsWorld(inflatedShape);
        //this.index = new ShapeIndex(this.inflatedShape, this.shape.scale);
    }
}
