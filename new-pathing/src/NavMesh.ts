import Vector2 from './Vector2';
import * as earcut from 'earcut';
//import { Tiles, Shape, ShapeIndex, drawShape } from './tiler.ts';
import Shape, { ShapeIndex } from './Shape';
import Tiles from './Tiles';

class TriangleCorner extends Vector2 {
    triangles: Array<Triangle> = [];
    index: number;
}

interface TriangleSide {
    points: Array<TriangleCorner>;
    triangles: Array<Triangle>;
    normal?: Vector2;
    center: Vector2;
}

interface Triangle {
    corners: Array<TriangleCorner>;
    index: number;
    sides: Array<TriangleSide>;
    neighbors: Array<Triangle>;
    center: Vector2;
}

export default class NavMesh {
    points: Array<TriangleCorner> = [];
    triangles: Array<Triangle> = [];
    sides: Array<Array<TriangleSide>> = [];
    sidesFlat: Array<TriangleSide> = [];
    shape: Shape;

    constructor(shape: Shape) {
        //this.scale = scale;
        //let shapes: Array<Shape> = Shape.fromTiles(tiles, scale, scale/2);
        //shapes.forEach(shape => shape.simplify());

        //extractBodies(tiles, width, [], scale);

        //this.shapes = shapes;
        this.shape = shape;

        let formattedPoints: Array<number> = [];
        this.points = shape.points.map((point, idx) => {
            formattedPoints.push(point.x);
            formattedPoints.push(point.y);
            let corner = new TriangleCorner(point.x, point.y);
            corner.index = idx;
            return corner;
        });
        let points: Array<TriangleCorner> = this.points;

        //var edges: any = {};

        //function getEdge(i0, i1) {
        //    if (i0 > i1) i1 = [i0, i0 = i1][0];
        //    return edges[i0][i1];
        //}


        let triangles: Array<Triangle> = [];
        let indices = earcut(formattedPoints);

        for (let i = 0; i < indices.length; i+=3) {
            let i0 = indices[i+0];
            let i1 = indices[i+1];
            let i2 = indices[i+2];

            let p0 = points[i0];
            let p1 = points[i1];
            let p2 = points[i2];

            let center = new Vector2( (p0.x + p1.x + p2.x)/3, (p0.y + p1.y + p2.y)/3 );
            let triangle: Triangle = {index: i/3, corners: [p0, p1, p2], sides: [], neighbors: [], center};
            triangles.push(triangle);

            triangle.corners.forEach(corner => corner.triangles.push(triangle));

            this.processSides(triangle);
        }

        triangles.forEach(triangle => {
            let neighbors: Array<Triangle> = [];
            triangle.sides.forEach(side => {
                side.triangles.forEach(neighbor => {
                    if ( triangle !== neighbor ) neighbors.push(neighbor);
                });
            });
            triangle.neighbors = neighbors;
        });

        this.triangles = triangles;

        //this.inflatedShape = this.shape.inflateShape(0.3);
        //this.index = new ShapeIndex(this.inflatedShape, this.shape.scale);
    }

    processSides(triangle: Triangle): void {
        if (triangle.sides.length) return;

        triangle.corners.forEach((corner: TriangleCorner, idx: number) => {
            let p1 = corner;
            let p2 = triangle.corners[ idx+1 > 2 ? 0 : idx+1 ];

            let i0 = p1.index;
            let i1 = p2.index;

            this.sides[i0] = this.sides[i0] || [];
            this.sides[i1] = this.sides[i1] || [];

            let side: TriangleSide = this.sides[p1.index][p2.index];

            if ( ! side ) {
                let direction = p1.vectorTo(p2);
                //let tp1 = p2.clone().subtract(p1.clone());
                //let normal = direction.rotate(Math.PI/2, origin).toUnitVector();
                let normal = direction.rotate(90).normalize();
                //normal = $V([Math.round(normal.x * 100) / 100, Math.round(normal.y * 100) / 100]);
                side = {triangles: [], points: [p1, p2], normal, center: Vector2.centerBetween(p1, p2)};
                this.sidesFlat.push(side);
            }

            side.triangles.push(triangle);
            triangle.sides.push(side);

            this.sides[i0][i1] = side;
            this.sides[i1][i0] = side;
        });
    }

    findTriangle(point: Vector2): Triangle {
        let triangle;
        this.triangles.some(t => {
            let bool = point.inTriangle(t.corners[0], t.corners[1], t.corners[2]);
            if (bool) triangle = t;
            return bool;
        });
        return triangle;
    }

    findPath(start: Vector2, end: Vector2): PointPath {
        let startTriangle = this.findTriangle(start);
        let endTriangle = this.findTriangle(end);

        if ( ! startTriangle || ! endTriangle ) { 
            console.error('Start or End triangle not found');
            return null;
        }

        let trianglePath: TrianglePath = TrianglePath.find(startTriangle, endTriangle);
        //var portals = initPortals(solution.path, start, this.end);
        //var path = funnel(portals);
        //return getPathNodes(path);

        let portals = initPortals(trianglePath, start, end);
        let pointPath: PointPath = funnel(portals);

        return pointPath;
    }

    draw(): void {
        /*////////////////////

        var canvas = <HTMLCanvasElement> document.getElementById('view');
        var ctx = canvas.getContext('2d');

        ctx.fillStyle="white";
        ctx.fillRect(0, 0, 1000, 1000);
        //ctx.clearRect(20,20,100,50);

        drawShape(this.shape);

        //let v1 = new Vector2(106.9, 82.7);
        //let v2 = new Vector2(273.1, 569.2);

        //let v1 = new Vector2(36.9, 22.7);
        //let v2 = new Vector2(532.1, 379.2);

        //let v1 = new Vector2(86.9, 218.2);
        //let v2 = new Vector2(545, 392.7);

        //let v1 = new Vector2(1.5 * sample, 5.5 * sample);
        //let v2 = new Vector2(7.5 * sample, 3.5 * sample);

        //let v1 = new Vector2(55, 306);
        //let v2 = new Vector2(245, 135);

        let v1 = new Vector2(463, 334);
        let v2 = new Vector2(65, 110);


        //[v1, v2].forEach(v => index.clipVector(v));

        let indices: Array<any> = [];
        this.index.castBetween(v1, v2, (index, cell) => {
            indices.push({index, cell});
            return false;
        });

        let sample = this.index.sample;
        for (let x = 0; x < this.index.width; ++x) {
            for (let y = 0; y < this.index.height; ++y) {
                ctx.beginPath();
                ctx.strokeStyle = 'teal';
                ctx.lineWidth = 1;
                ctx.rect(x * sample, y * sample, sample, sample);
                ctx.stroke();
            }
        }

        indices.forEach(index => {
            ctx.beginPath();
            ctx.fillStyle = 'teal';
            ctx.rect(index.cell.x * sample, index.cell.y * sample, sample, sample);
            ctx.fill();
        });

        let sides = [];
        indices.forEach(index => {
            sides = [...sides, ...index.index.sides];
        });

        sides = Array.from(new Set(sides));
        sides.forEach(side => {
            ctx.lineWidth = 6;
            ctx.strokeStyle = 'yellow';
            ctx.beginPath();
            ctx.moveTo(side.points[0].x, side.points[0].y);
            ctx.lineTo(side.points[1].x, side.points[1].y);
            ctx.stroke();
        });

        ctx.strokeStyle = 'black';
        ctx.lineWidth = 2;
        ctx.beginPath();
        ctx.moveTo(v1.x, v1.y);
        ctx.lineTo(v2.x, v2.y);
        ctx.stroke();

        drawShape(this.inflatedShape, 'red');


        ctx.strokeStyle="#000000";
        ctx.lineWidth = 1;
        for (var i = 0; i < this.triangles.length; ++i) {
            ctx.fillStyle = "black";
            var triangle = this.triangles[i];

            ctx.beginPath();
            ctx.moveTo(triangle.corners[0].x, triangle.corners[0].y);
            ctx.lineTo(triangle.corners[1].x, triangle.corners[1].y);
            ctx.lineTo(triangle.corners[2].x, triangle.corners[2].y);
            ctx.lineTo(triangle.corners[0].x, triangle.corners[0].y);
            ctx.stroke();

            //ctx.fillStyle="black";
            ctx.font = "10px serif";
            ctx.fillText(triangle.index.toString(), triangle.center.x-3, triangle.center.y);
        }

        this.sidesFlat.forEach(side => {
            ctx.fillStyle = "red";
            ctx.beginPath();
            ctx.arc(side.center.x, side.center.y, 2, 0, Math.PI*2);
            ctx.closePath();
            ctx.fill();

            ctx.strokeStyle = "blue";
            ctx.beginPath();
            ctx.moveTo(side.center.x, side.center.y);
            let scale = 9;
            ctx.lineTo( side.center.x + (side.normal.x*scale), side.center.y + (side.normal.y*scale) );
            ctx.closePath();
            ctx.stroke();
        });

        *////////////

    }

    /*addTriangleToCorner(triangle: Triangle, corner: TriangleCorner) {
        var direction = p2.subtract(p1);

        if (i0 > i1) i1 = [i0, i0 = i1][0];
        edges[i0] = edges[i0] || {};
        edges[i0][i1] = edges[i0][i1] || {indices: [i0, i1], normal: normal, triangles: [], points: [points[i0], points[i1]]};
        var edge = edges[i0][i1];
        if (edge.triangles.indexOf(triangle) < 0) edge.triangles.push(triangle);
        return edge;
    };*/
}


//type Node = any;

function distance(a: TrianglePathNode, b: TrianglePathNode): number {
    return a.triangle.center.distance(b.triangle.center);
};

export class TrianglePathNode {
    index: number = null;
    triangle: Triangle = null;

    hcost: number;
    gcost: number;
    fcost: number;
    p: TrianglePathNode;

    meta: any = {};

    constructor(triangle: Triangle) {
        this.triangle = triangle;
    }

    costs(destinationNode: TrianglePathNode, pNode?: TrianglePathNode): void {
        this.hcost = distance(this, destinationNode);
        this.gcost = (pNode ? pNode.gcost + distance(this, pNode) : 0);
        this.fcost = this.hcost + this.gcost;
        (pNode ? this.p = pNode : null);
    };
}

export class PointPathNode extends Vector2 {
    index: number = null;
}

export class PointPath {
    nodes: Array<PointPathNode>;
}

export class TrianglePath {
    nodes: Array<TrianglePathNode> = [];

    constructor() {}

    static find(startTriangle: Triangle, endTriangle: Triangle): any {
        //var idIndex = 0;
        var OpenList   = [];
        var ClosedList = [];

        let TrianglePathNodes = new Map();
        function getNode(triangle: Triangle): TrianglePathNode {
            let node = <TrianglePathNode> TrianglePathNodes.get(triangle);
            if (node) return node;
            node = new TrianglePathNode(triangle);
            TrianglePathNodes.set(triangle, node);
            return node;
        }

        let startNode = getNode(startTriangle);
        let endNode   = getNode(endTriangle);

        startNode.costs(endNode);
        OpenList.push(startNode);

        while (true) {
            let parentNode = null;

            if (OpenList.length <= 0) break;

            OpenList.forEach(node => {
                if ( ! parentNode ) parentNode = node;
                else if (node.fcost < parentNode.fcost) parentNode = node;
            });

            OpenList.splice(OpenList.indexOf(parentNode), 1);
            ClosedList.push(parentNode);

            if (parentNode == endNode) {
                let path = new TrianglePath();
                let node = parentNode;
                while (node) {
                    path.nodes.push(node);
                    if (node.triangle.index == startNode.triangle.index) {
                        node = null;
                        continue;
                    }
                    if (node.p) node = node.p;
                }
                path.nodes.reverse();
                path.nodes.forEach((node, idx) => node.index = idx);
                return path;
            }

            parentNode.triangle.neighbors.forEach(triangle => {
                let node = getNode(triangle);

                if (ClosedList.indexOf(node) > -1) return;
                var tileIndex = OpenList.indexOf(node);
                if (tileIndex > -1) {
                    let gcost = node.gcost + distance(node, parentNode);
                    node = OpenList[tileIndex];
                    if (gcost < node.gcost) {
                        node.p = parentNode;
                        node.gcost = gcost;
                        node.fcost = gcost + node.hcost;
                    }
                } else {
                    node.costs(endNode, parentNode);
                    OpenList.push(node);
                }
            });
        }

        return null;
    }

    /*
    draw() {
        var canvas = <HTMLCanvasElement> document.getElementById('view');
        var ctx = canvas.getContext('2d');

        this.nodes.forEach((node, idx) => {
            if (idx+1 == this.nodes.length) return;
            let nextNode = this.nodes[idx+1];

            ctx.strokeStyle = 'green';
            ctx.lineWidth = 1;
            ctx.beginPath();
            ctx.moveTo(node.x, node.y);
            ctx.lineTo(nextNode.x, nextNode.y);
            ctx.stroke();
        });
    }
    */
}



/* Uncomment to test.
var impassableTerrain = [1];
var TestBoard = [
    [0, 0, 0, 0], 
    [1, 1, 1, 0], 
    [1, 1, 1, 0], 
    [0, 0, 0, 0]
];
var path = FindPath(TestBoard, 0, 0, 3, 0, impassableTerrain);
if (path) console.log(JSON.stringify(path, null, 2));
else console.log("No path was found.");
*/

//function initPortals(path: Array<PathNode>, startPos: Vector2, endPos: Vector2) {
function initPortals(path: TrianglePath, startPos: Vector2, endPos: Vector2) {
    console.log(path);
    startPos = startPos || path.nodes[0].triangle.center;
    endPos = endPos || path.nodes[path.nodes.length-1].triangle.center;

    var portals = []; 

    portals.push({
        left: startPos,
        right: startPos
    }); 

    path.nodes.forEach(function (node, idx) {
        let triangle = node.triangle;
        if (idx == 0) return;
    
        var previousTriangle = path.nodes[idx-1].triangle;

        triangle.sides.forEach(side => {
            side.triangles.forEach(triangle => {
                if (triangle != previousTriangle) return;

                let corners = triangle.corners.slice(0);
                while (corners[0].index == side.points[0].index || corners[0].index == side.points[1].index) {
                    corners.push(corners.shift());
                }   

                portals.push({
                    //left: points[pointIndices[2]],
                    //right: points[pointIndices[1]],
                    left: corners[2],
                    right: corners[1]
                }); 
            }); 
        }); 
    }); 

    portals.push({
        left: endPos,
        right: endPos
    }); 

    return portals;
}



function funnel(portals): PointPath {
    var pts = []; 

    // Init scan state
    var portalApex, portalLeft, portalRight;
    var apexIndex = 0;
    var leftIndex = 0;
    var rightIndex = 0;

    portalApex  = portals[0].left;
    portalLeft  = portals[0].left;
    portalRight = portals[0].right;

    // Add start point.
    pts.push(portalApex);

    for (var i = 1; i < portals.length; i++) {
        var left = portals[i].left;
        var right = portals[i].right;

        // Update right vertex.
        if (triarea2(portalApex, portalRight, right) <= 0.0) {
            if (vequal(portalApex, portalRight) || triarea2(portalApex, portalLeft, right) > 0.0) {
                // Tighten the funnel.
                portalRight = right;
                rightIndex = i;
            } else {
                // Right over left, insert left to path and restart scan from portal left point.
                pts.push(portalLeft);
                // Make current left the new apex.
                portalApex = portalLeft;
                apexIndex = leftIndex;
                // Reset portal
                portalLeft = portalApex;
                portalRight = portalApex;
                leftIndex = apexIndex;
                rightIndex = apexIndex;
                // Restart scan
                i = apexIndex;
                continue;
            }   
        }   

        // Update left vertex.
        if (triarea2(portalApex, portalLeft, left) >= 0.0) {
            if (vequal(portalApex, portalLeft) || triarea2(portalApex, portalRight, left) < 0.0) {
                // Tighten the funnel.
                portalLeft = left;
                leftIndex = i;
            } else {
                // Left over right, insert right to path and restart scan from portal right point.
                pts.push(portalRight);
                // Make current right the new apex.
                portalApex = portalRight;
                apexIndex = rightIndex;
                // Reset portal
                portalLeft = portalApex;
                portalRight = portalApex;
                leftIndex = apexIndex;
                rightIndex = apexIndex;
                // Restart scan
                i = apexIndex;
                continue;
            }   
        }   
    }   

    if ((pts.length === 0) || (!vequal(pts[pts.length - 1], portals[portals.length - 1].left))) {
        // Append last point to path.
        pts.push(portals[portals.length - 1].left);
    }

    let path = new PointPath();
    path.nodes = pts.map((point, idx) => {
        let pathNode = new PointPathNode(point.x, point.y);
        pathNode.index = idx;
        return pathNode;
    });

    console.log(path);

    return path;
};

    function triarea2(a, b, c) {
        /*var ax = b.x - a.x;
        var az = b.z - a.z;
        var bx = c.x - a.x;
        var bz = c.z - a.z;*/
        var ax = b.x - a.x;
        var ay = b.y - a.y;
        var bx = c.x - a.x;
        var by = c.y - a.y;

        return bx * ay - ax * by; 
    }   

    function vequal(a, b) {
        return distanceToSquared(a, b) < 0.00001;
    }   

    var distanceToSquared = function (a, b) {
        var dx = a.x - b.x;
        var dy = a.y - b.y;
        var dz = 0 - 0;

        return dx * dx + dy * dy + dz * dz; 
    };  
   
//function getPathNodes(path, groupId) {
function getPathNodes(path): Array<Vector2> {
    var nodes: Array<Vector2> = []; 
    path.forEach(function (node, idx) {
        if (idx == 0) return;
        if (node.x == path[idx-1].x && node.y == path[idx-1].y) return;
        nodes.push(new Vector2(node.x, node.y));
    }); 
    return nodes;
}
