import Vector2 from './Vector2';
import * as _ from 'lodash';
import Renderer from './Renderer';
import MathUtils from './MathUtils';

export class ShapeSide {
    points: Array<ShapeCorner>;
    normal: Vector2;
    index: number = null;

    constructor(v1: ShapeCorner, v2: ShapeCorner) {
        this.points = [v1, v2];
        this.normal = v2.clone().subtract(v1).normalize().rotate(-90);
    }
}

export class ShapeCorner extends Vector2 {
    sides: Array<ShapeSide> = [];
    normal: Vector2 = null;
    index: number = null;

    constructor(x, y) {
        super(x, y);
    }
}

export default class Shape {
    points: Array<ShapeCorner> = [];
    sides: Array<ShapeSide> = [];
    scale: number;
    margin: number;
    scaledFrom: Shape;

    constructor(points: Array<Vector2>, scale: number = 1, offset: number = 0) {
        this.scale = scale;

        points.forEach((point, idx) => {
            let corner = new ShapeCorner(point.x, point.y);
            corner.index = idx;

            corner.x *= scale;
            corner.y *= scale;

            corner.x += offset;
            corner.y += offset;
            this.points.push(corner);
        });

        // These calls can be made optional, in the future. Selfish convenience, for now.
        this.simplify();
        this.generateSides();
        this.generateNormals();
    }

    // Reduce straight edges of multiple segments into single segments
    simplify() {
        let newPoints = [];

        this.points.forEach((corner, idx) => {
            if (newPoints.length === 0) newPoints.push(corner);
            else {
                let prevIdx = newPoints.length-1;
                let previousCorner  = newPoints[prevIdx];
                let previousCorner2 = newPoints[prevIdx-1];

                if (idx === this.points.length-1) {
                    let firstCorner = this.points[0];
                    if (corner.x === firstCorner.x && corner.y === firstCorner.y) {
                        if      (previousCorner2 && corner.x === previousCorner2.x) newPoints.pop();
                        else if (previousCorner2 && corner.y === previousCorner2.y) newPoints.pop();
                        return;
                    }
                }

                if      (corner.x === previousCorner.x && previousCorner2 && corner.x === previousCorner2.x) newPoints[prevIdx] = corner;
                else if (corner.y === previousCorner.y && previousCorner2 && corner.y === previousCorner2.y) newPoints[prevIdx] = corner;
                else    newPoints.push(corner);
            }
        });

        this.points = newPoints;
    }

    generateSides() {
        this.sides.length = 0;
        this.points.forEach(point => point.sides.length = 0);

        this.points.forEach((point, idx) => {
            let nextPoint = this.points[ (idx+1)%this.points.length ];
            let side = new ShapeSide(point, nextPoint);

            point.sides.push(side);
            nextPoint.sides.push(side);
            this.sides.push(side);
        });
    }

    generateNormals() {
        this.points.forEach(point => {
            let normal = new Vector2(0, 0);
            point.sides.forEach(side => normal.add(side.normal));
            normal.div(2).normalize();
            point.normal = normal;
        });

    }

    // Messy implementation for a specific need. Probably should pull this
    // method out and rebrand it as a util.
    inflateShape(margin: number): Shape {
        if (this.scaledFrom) throw new Error('This shape is a result of a previous expansion.');

        let expandedShape = new Shape(this.points.map(corner => {
            /*let cloned = _.cloneWith(corner, (value: ShapeCorner, key) => {
                if (key === 'normal') return value.clone();
                return value;
            });*/
            let cloned = _.clone(corner);
            cloned.normal = corner.normal.clone();

            let vector = new Vector2(cloned.x, cloned.y);
            vector.add(cloned.normal.mult(this.scale * margin).clone());

            cloned.x = vector.x;
            cloned.y = vector.y;

            return cloned;
        }));

        expandedShape.scale = this.scale;
        expandedShape.scaledFrom = this;
        expandedShape.margin = margin;

        return expandedShape;
    }
}

type CellIndex = {points: Array<ShapeCorner>, sides: Array<ShapeSide>};

export class ShapeIndex {
    index: Array<CellIndex> = [];
    width: number;
    height: number;
    sample: number;
    shape: Shape;

    constructor(shape: Shape, sample: number) {
        this.shape = shape;
        this.buildIndex(shape, sample);
    }

    rebuildIndex(sample: number) {
        this.buildIndex(this.shape, sample);
    }

    buildIndex(shape: Shape, sample: number) {
        this.index.length = 0;
        this.sample = sample;
        let width = 0;
        let height = 0;

        shape.points.forEach(point => {
            let x = Math.floor(point.x/sample);
            let y = Math.floor(point.y/sample);

            x+1 > width  ? width  = x+1 : null;
            y+1 > height ? height = y+1 : null;
        });

        this.width = width;
        this.height = height;

        let length = width * height;
        for (let l = 0; l < length; ++l) {
            this.index.push({points: [], sides: []});
        }

        shape.points.forEach(point => {
            let x = Math.floor(point.x/sample);
            let y = Math.floor(point.y/sample);
            this.index[this.getIndexOf(x, y)].points.push(point);
            //points.push({x, y, point});
        });  

        //shape.points.forEach(point => {
        //    this.index[this.getIndexOf(point.x, point.y)] = point.point;
        //});

        shape.sides.forEach(side => {
            this.castBetween(side.points[0], side.points[1], index => {
                index.sides.push(side);
                return false;
            });
        });
    }

    getIndexOf(x: number, y: number): number {
        return (this.width*y)+x;
    }

    getIndexAt(x: number, y: number): CellIndex {
        let index = this.index[this.getIndexOf(x, y)];
        return index;
    }

    /*
    _bresenham(v1: Vector2, v2: Vector2): Array<ShapeCorner> {
        console.log('bresenham()');
        let corners = [];

        if (v2.x < v1.x) [v1, v2] = [v2, v1];

        let dx  = Math.abs(v2.x - v1.x);
        let dy  = Math.abs(v2.y - v1.y);
        let sx  = (v1.x < v2.x) ? 1 : -1;
        let sy  = (v1.y < v2.y) ? 1 : -1;
        let err = dx - dy;

        let ox;
        let oy;

        //console.log(dx, dy, dx, sy, err);

        v1.log();
        v2.log();

        while (true) {
            this.result.push(v1.clone());

            corners = [...corners, ...this.getCornersAt(v1.x, v1.y)];

            if ( (ox !== undefined && oy !== undefined ) && (ox !== v1.x && oy !== v1.y) ) {
                corners = [...corners, this.getCornersAt(v1.x-1, v1.y)];
                //if ( (v1.y - oy) < 0 ) corners = [...corners, ...this.getCornersAt(v1.x, v1.y+1)];
                //else                   corners = [...corners, ...this.getCornersAt(v1.x, v1.y-1)];

                //console.log('Y:', oy, v1.y);
                //if ( (v1.y - oy) < 0 ) { this.result.push(v1.clone().add(new Vector2(-1, 0))); this.result.push(v1.clone().add(new Vector2(0, 1))); }
                //else                   { this.result.push(v1.clone().add(new Vector2(-1, 0))); this.result.push(v1.clone().add(new Vector2(0, -1))); }
            }

            if ( (v1.x == v2.x) && ( v1.y == v2.y ) ) break;

            ox = v1.x; oy = v1.y;

            var e2 = 2*err;
            if (e2 > -dy) { err -= dy; v1.x += sx; }
            if (e2 <  dx) { err += dx; v1.y += sy; }

            //v1.x += sx;
            //v1.y += sy;
        }

        return corners;
    }
    */

    /*
    _bresenham(v1: Vector2, v2: Vector2): Array<ShapeCorner> {
        let difX = v2.x - v1.x;
        let difY = v2.y - v1.y;
        let dist = Math.ceil(Math.abs(difX) + Math.abs(difY));

        let dx = difX / dist;
        let dy = difY / dist;

        for (let i = 0; i <= dist; i++) {
            let x = Math.floor(v1.x + dx * i);
            let y = Math.floor(v1.y + dy * i);
            //draw(x,y);
            this.result.push(new Vector2(x, y));
        }

        return [];
        //return true;
    }
    */

    // Frac if fractional part of float, for example, Frac(1.3) = 0.3, Frac(-1.7)=0.7
    frac0(f) {
        //return Math.abs(f % 1);
        return f % 1;
    }

    frac1(f) {
        return 1 - f + Math.floor(f);
    }

    /*clipVector(v: Vector2) {
        v.x = Math.max(0, v.x);
        v.x = Math.min(v.x, (this.width-1) * this.sample);

        v.y = Math.max(0, v.y);
        v.y = Math.min(v.y, (this.height-1) * this.sample);
    }*/

    castBetween(v1: Vector2, v2: Vector2, cb: (index: CellIndex, cell?: Vector2) => boolean ): void {
        v1 = v1.clone();
        v2 = v2.clone();

        // Handle points directly on an edge
        let direction = v2.clone().subtract(v1);
        if (v2.x % this.sample === 0) {
            v2.x = v2.x + (-Math.sign(direction.x)*0.001);
        }

        if (v2.y % this.sample === 0) {
            v2.y = v2.y + (-Math.sign(direction.y)*0.001);
        }

        v1 = v1.div(this.sample);
        v2 = v2.div(this.sample);

        MathUtils.CohenSutherlandLineClipAndDraw(v1, v2, this.width-0.01, this.height-0.01);
        
        /*
        let dx = v2.x - v1.x;
        let tDeltaX = 1 / dx;
        let tMaxX = tDeltaX * (1 - this.frac(v1.x));

        let dy = v2.y - v1.y;
        let tDeltaY = 1 / dy;
        let tMaxY = tDeltaY * (1 - this.frac(v1.y));
        */

        let tDeltaX, tMaxX;
        let dx = Math.sign(v2.x - v1.x);
        if ( dx !== 0 ) tDeltaX = Math.min(dx / (v2.x - v1.x), 10000000);
        else tDeltaX = 10000000;
        if ( dx > 0 ) tMaxX = tDeltaX * this.frac1(v1.x);
        else tMaxX = tDeltaX * this.frac0(v1.x);

        let tDeltaY, tMaxY;
        let dy = Math.sign(v2.y - v1.y);
        if ( dy !== 0 ) tDeltaY = Math.min(dy / (v2.y - v1.y), 10000000);
        else tDeltaY = 10000000;
        if ( dy > 0 ) tMaxY = tDeltaY * this.frac1(v1.y);
        else tMaxY = tDeltaY * this.frac0(v1.y);


        let x = v1.x;
        let y = v1.y;
        let stepX = dx > 0 ? 1 : -1;
        let stepY = dy > 0 ? 1 : -1;

        let destX = Math.floor(v2.x);
        let destY = Math.floor(v2.y);

        /*
        console.log('------------------');
        console.log(v1.x, v1.y);
        console.log(v2.x, v2.y);
        console.log(dx, dy);
        console.log('');
        console.log(x, y);
        console.log(destX, destY);
        console.log('Delta:', tDeltaX, tDeltaY);
        console.log('tMax:', tMaxX, tMaxY);
        */

        while (true) {
            let fx = Math.floor(x);
            let fy = Math.floor(y);

            if (fx+1 > this.width || fy+1 > this.height) { console.log('B1'); break; }
            if (fx < 0 || fy < 0) { console.log('B2'); break; }

            let index = this.getIndexAt(fx, fy);
            if ( ! index ) console.log('!?!?!?', this.width, this.height, fx, fy);

            let ctx = Renderer.ctx();
            ctx.beginPath();
            ctx.rect(fx*this.sample, fy*this.sample, this.sample, this.sample);
            ctx.strokeStyle = 'purple';
            ctx.lineWidth = 6;
            ctx.stroke();

            if ( cb(index, new Vector2(fx, fy)) ) break;
            //if (tMaxX > 1 || tMaxY > 1) break;

            if (destX === fx && destY === fy) break;

            if (tMaxX < tMaxY)
            {
                tMaxX = tMaxX + (tDeltaX);
                x = x + stepX;
            }
            else
            {
                tMaxY = tMaxY + (tDeltaY);
                y = y + stepY;
            }
        }

            let ctx = Renderer.ctx();
            ctx.beginPath();
            ctx.rect(destX*this.sample, destY*this.sample, this.sample, this.sample);
            ctx.strokeStyle = 'orange';
            ctx.lineWidth = 6;
            ctx.stroke();

    }

    /*
    getIndicesBetween(v1: Vector2, v2: Vector2): Array<ShapeCorner> {
        //let startX = v1.x / this.sample;
        //let startY = v1.y / this.sample;

        //let endX = v2.x / this.sample;
        //let endY = v2.y / this.sample;

        //let corners = this._bresenham(new Vector2(startX, startY), new Vector2(endX, endY));
        //let corners = this._bresenham(v1, v2);
        
        let corners = [];
        return corners;
    }
    */
}
