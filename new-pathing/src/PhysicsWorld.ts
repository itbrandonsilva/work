import Shape, { ShapeIndex } from './Shape';
import Vector2 from './Vector2';
import MathUtils from './MathUtils';

export default class PhysicsWorld {
    walls: ShapeIndex;
    shape: Shape;

    constructor(shape: Shape) {
        console.log('Shape scale:', shape.scale);
        this.shape = shape;
        this.walls = new ShapeIndex(shape, shape.scale);
        console.log("SHAPE INDEX");
    }

    castBetween(v1: Vector2, v2: Vector2, cb: (collision: any) => boolean ): void {
        let collisionFound = false;

        this.walls.castBetween(v1, v2, (index, cell) => {
            let collisions = [];

            index.sides.forEach(side => {
                let intersection = MathUtils.getPointOfIntersectionBetweenSegments(v1, v2, side.points[0], side.points[1]);
                if (intersection) {
                    collisionFound = true;
                    collisions.push(intersection);
                }
            });

            let done = false;
            collisions.forEach(coll => {
                if (done) return;
                done = cb(coll);
            });

            return done;
        });

        if ( ! collisionFound ) cb(null);
    }
}
