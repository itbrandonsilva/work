import Vector2 from './Vector2';

const INSIDE = 0; // 0000
const LEFT = 1;   // 0001
const RIGHT = 2;  // 0010
const BOTTOM = 4; // 0100
const TOP = 8;    // 1000

export default class MathUtils {
    static getPointOfIntersectionBetweenSegments(p: Vector2, p2: Vector2, q: Vector2, q2: Vector2): Vector2 {
        //if (isPointOnSegment(p,  q, q2)) return p;
        //if (isPointOnSegment(p2, q, q2)) return p2;

        var r = p2.clone().subtract(p);
        var s = q2.clone().subtract(q);

        var uNumerator = q.clone().subtract(p).cross(r);
        var denominator = r.clone().cross(s);

        if (uNumerator == 0 && denominator == 0) {
            // They are coLlinear
            return;
        
            /*// Do they touch? (Are any of the points equal?)
            if (p.eql(q) || p.eql(q2) || p2.eql(q) || p2.eql(q2)) {
                return true
            }
            // Do they overlap? (Are all the point differences in either direction the same sign)
            // Using != as exclusive or
            return ((q.x - p.x < 0) != (q.x - p2.x < 0) != (q2.x - p.x < 0) != (q2.x - p2.x < 0)) || 
                ((q.y - p.y < 0) != (q.y - p2.y < 0) != (q2.y - p.y < 0) != (q2.y - p2.y < 0));*/
        }   

        if (denominator == 0) {
            // lines are paralell
            return;
        }   

        var t = q.clone().subtract(p).cross(s) / denominator;
        var u = uNumerator / denominator;

        //return (t >= 0) && (t <= 1) && (u >= 0) && (u <= 1);
        if ( (t >= 0) && (t <= 1) && (u >= 0) && (u <= 1) ) { 
            var intersectionPoint = p.clone().add(r.clone().multiply(t));
            //console.log(JSON.stringify(p));
            return intersectionPoint;
        }   
    }

    static ComputeOutCode(v: Vector2, width: number, height: number): number {
        let code = INSIDE;       // initialised as being inside of [[clip window]]

        if (v.x < 0)             // to the left of clip window
            code |= LEFT;
        else if (v.x > width)    // to the right of clip window
            code |= RIGHT;
        if (v.y < 0)             // below the clip window
            code |= BOTTOM;
        else if (v.y > height)   // above the clip window
            code |= TOP;

        return code;
    }

    static CohenSutherlandLineClipAndDraw(v1: Vector2, v2: Vector2, width: number, height: number): void {
        // compute outcodes for P0, P1, and whatever point lies outside the clip rectangle
        let outcode0: number = MathUtils.ComputeOutCode(v1, width, height);
        let outcode1: number = MathUtils.ComputeOutCode(v2, width, height);
        let accept = false;

        while (true) {
            if (! (outcode0 | outcode1)) { // Bitwise OR is 0. Trivially accept and get out of loop
                accept = true;
                break;
            } else if (outcode0 & outcode1) { // Bitwise AND is not 0. Trivially reject and get out of loop
                break;
            } else {
                // failed both tests, so calculate the line segment to clip
                // from an outside point to an intersection with clip edge
                let x: number;
                let y: number;

                // At least one endpoint is outside the clip rectangle; pick it.
                let outcodeOut: number = outcode0 ? outcode0 : outcode1;

                // Now find the intersection point;
                // use formulas y = y0 + slope * (x - x0), x = x0 + (1 / slope) * (y - y0)
                if (outcodeOut & TOP) {           // point is above the clip rectangle
                    x = v1.x + (v2.x - v1.x) * (height - v1.y) / (v2.y - v1.y);
                    y = height;
                } else if (outcodeOut & BOTTOM) { // point is below the clip rectangle
                    x = v1.x + (v2.x - v1.x) * (0 - v1.y) / (v2.y - v1.y)//                     x0 + (x1 - x0) * (ymin - y0) / (y1 - y0);
                    y = 0;
                } else if (outcodeOut & RIGHT) {  // point is to the right of clip rectangle
                    x = width;
                    y = v1.y + (v2.y - v1.y) * (width - v1.x) / (v2.x - v1.x);//                                      y0 + (y1 - y0) * (xmax - x0) / (x1 - x0);
                } else if (outcodeOut & LEFT) {   // point is to the left of clip rectangle
                    x = 0;
                    y = v1.y + (v2.y - v1.y) * (0 - v1.x) / (v2.x - v1.x);
                }

                // Now we move outside point to intersection point to clip
                // and get ready for next pass.
                if (outcodeOut == outcode0) {
                    v1.x = x;
                    v1.y = y;
                    outcode0 = MathUtils.ComputeOutCode(v1, width, height);
                } else {
                    v2.x = x;
                    v2.y = y;
                    outcode1 = MathUtils.ComputeOutCode(v2, width, height);
                }
            }
        }
        //if (accept) {
                   // Following functions are left for implementation by user based on
                   // their platform (OpenGL/graphics.h etc.)
                   //DrawRectangle(xmin, ymin, xmax, ymax);
                   //LineSegment(x0, y0, x1, y1);
        //}
    }

}


// Compute the bit code for a point (x, y) using the clip rectangle
// bounded diagonally by (xmin, ymin), and (xmax, ymax)

// ASSUME THAT xmax, xmin, ymax and ymin are global constants.


// Cohen–Sutherland clipping algorithm clips a line from
// P0 = (x0, y0) to P1 = (x1, y1) against a rectangle with 
// diagonal from (xmin, ymin) to (xmax, ymax).

