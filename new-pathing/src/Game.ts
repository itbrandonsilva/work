import Vector2 from './Vector2';
import Character from './Character';
import World from './World';

class Player {
    selected: Array<Character> = [];
    game: Game;

    constructor(game) {
        this.game = game;
    }

    selectAll() {
        this.selected.length = 0;
        this.game.characters.forEach(character => this.selected.push(character));
    }
}

export default class Game {
    characters: Array<Character> = [];
    world: World;
    players: Array<Player> = [];
    nextId: number = 0;

    constructor(world: World) {
        this.world = world;
    }

    genId() {
        return ++this.nextId;
    }

    addCharacter(character: Character) {
        character.setWorld(this.world);
        character.id = this.genId();
        this.characters.push(character);
    }

    assignPath(character: Character, target: Vector2) {
        let path = this.world.navigation.findPath(character.position, target);
        if ( ! path ) return;
        character.setPath(path);
    }

    step(ms: number) {
        this.characters.forEach(character => {
            character.steer(ms);
            character.applyVelocity(ms);
        });
    }
}

//export default class AISystem {
//    agents: Array<Agent> = [];
    //selected: Array<Agent> = [];
    //nextId: number = 1;
    //groups: Array<Array<Agent>> = [];
    //navMesh: NavMesh;

    //constructor(navMesh: NavMesh) {
    //    this.navMesh = navMesh
    //}

    //addAgent(agent: Agent) {
    //    agent.id = this.newId();
    //    this.agents.push(agent);
    //}

    //newId() {
    //    return this.nextId++;
    //}

    //selectRect(topLeft: Vector2, bottomRight: Vector2) {
    //    this.clearSelection();
    //    this.agents.forEach(agent => {
    //        agent.position.inRect(topLeft, bottomRight) ? this.selected.push(agent) : null;
    //    });
    //}


/*

    assignPathToSelection(target: Vector2) {
        this.selected.forEach(agent => this.assignPathToAgent(agent, target));
    }

    assignPathToAgent(agent: Agent, target: Vector2) {
        let path = this.navMesh.findPath(agent.position, target);
        if ( ! path ) return;
        agent.setPath(path);
    }

    clearSelection() {
        this.selected.length = 0;
    }

    draw() {
        this.navMesh.draw();
        this.agents.forEach(agent => agent.draw());
    }

    step() {
        this.agents.forEach(agent => agent.step());
    }
}

*/
