type IsSolidCallback = (value: any) => boolean;
interface Edge {
    processed?: boolean;
}

export default class Tiles {
    private _isSolid: IsSolidCallback;
    tiles: Array<any>;
    width: number;
    height: number;

    constructor(width: number, isSolid: IsSolidCallback, tiles: Array<any>) {
        this.tiles = tiles;
        this.width = width;
        this.height = tiles.length/width;
        this._isSolid = isSolid;
    }

    getAt(x: number, y: number): number {
        if ( x < 0 || y < 0 ) return undefined;
        if ( x+1 > this.width || y+1 > this.height ) return undefined;
        return this.tiles[(y*this.width)+x];
    }

    isSolidAt(x: number, y: number) {
        let value = this.getAt(x, y);
        if ( value === undefined ) return true;
        let isSolid = this._isSolid(value);
        return isSolid;
    }
}
