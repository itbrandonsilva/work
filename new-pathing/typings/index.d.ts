/// <reference path="globals/es6-collections/index.d.ts" />
/// <reference path="globals/lodash/index.d.ts" />

interface Math {
    sign: (number: number) => number;
}

declare module 'earcut' {
    // declare an any type?
    var x: any;

    // default export is of type x (any)?
    export = x;
}

interface ArrayConstructor {
    from: (array: Set<any>) => Array<any>;
}
