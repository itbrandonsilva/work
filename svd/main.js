var ctx = document.getElementById('render').getContext('2d');

function drawDot(point, radius) {
    radius = radius || 10;
    ctx.beginPath();
    ctx.arc(point.x, point.y, radius, 0, 2*Math.PI);
    ctx.fill();
}

ctx.fillStyle = "teal";
ctx.fillRect(0, 0, 1024, 768);

var points = [
    [700, 360],
    [70, 90],
    [135, 150],
    [90, 30],
    [175, 130],
    [200, 50],
    [500, 75],
    [300, 300],
    [135, 500],
    [303, 100],
    [275, 275],
    [200, 50],
];

var center;
var total = [0, 0];
ctx.fillStyle = 'black';
/*points.forEach((p) => {
    total[0] += p[0];
    total[1] += p[1];
    drawDot(p[0], p[1])
});*/

var numAgents = 100;

center = (() => {
    total[0] /= points.length;
    total[1] /= points.length;
    return total;
})();

console.log(center);
ctx.fillStyle = 'yellow';
//drawDot(center[0], center[1]);

var drawLine = function (s, e) {
    ctx.beginPath();
    ctx.moveTo(s.x, s.y);
    ctx.lineTo(e.x, e.y);
    ctx.stroke();
};

ctx.fillStyle = 'red';

/*
*/

// http://www.javascriptkit.com/script/script2/quadratic_cal.shtml

function solveQuadratic(a, b, c) {
        var fba1 = (-b + Math.sqrt((b*b) - (4 * a * c))) / (2 * a);
        var fba2 = (-b - Math.sqrt((b*b) - (4 * a * c))) / (2 * a);

        var ing1 = ((a*fba1*fba1*fba1)/3)+((b*fba1*fba1)/2)+(c*fba1);
        var ing2 = ((a*fba2*fba2*fba2)/3)+((b*fba2*fba2)/2)+(c*fba2);
        var inte = ing1-ing2;

        let area;
        if (inte > 0) area = inte;
        if (inte < 0) area = -inte;
        if (inte == 0) area = 0;

        let sec_dx = (-b)/(2*a);
        let sec_dy = (a*sec_dx*sec_dx)+(b*sec_dx)+(1+c);
        //sec_dy = Math.round(sec_dy*100)/100;

        console.log(a, b, c);
        console.log('Area?:', area);

        console.log('??:', fba1);
        console.log('??:', fba2);
        console.log('sec_dy:', sec_dy);


    return {
        fba1: fba1,
        fba2: fba2,
        inte: area,
        sec_dy: sec_dy,
    };
}

class GeoUtils {
    static pointInBound(bps, p) {
        var inBound = false;
        bps.forEach((bp1, idx) => {
            var bp2 = bps[(idx+1)%bps.length];
            if ( ((bp1.y > p.y) != (bp2.y > p.y)) && (p.x < (bp2.x - bp1.x) * (p.y - bp1.y) / (bp2.y - bp1.y) + bp1.x) ) {
                inBound = !inBound;
            }
        });
        return inBound;
    }

    static pointInBoundDepth(bps, p, depth) {
        depth = depth == undefined ? 20 : depth;
        var inBound;

        [
            p.clone(),
            p.clone().addX( depth),
            p.clone().addX(-depth),
            p.clone().addY( depth),
            p.clone().addY(-depth),
        ].some(function (point) {
            inBound = GeoUtils.pointInBound(bps, point);
            return !inBound;
        });

        return inBound;
    }

    static lineSegmentIntersection(r0, r1, a, b) {
        var s1 = r1.clone().subtract(r0);
        var s2 = b.clone().subtract(a);

        var s = (-s1.y * (r0.x - a.x) + s1.x * (r0.y - a.y)) / (-s2.x * s1.y + s1.x * s2.y);
        var t = (s2.x * (r0.y - a.y) - s2.y * (r0.x - a.x)) / (-s2.x * s1.y + s1.x * s2.y);

        if (s >= 0 && s <= 1 && t >= 0 && t <= 1)
        {
            // Collision detected
            // Return the point of intersection
            var intersection = new Vector2(r0.x + (t * s1.x), r0.y + (t * s1.y));
            return intersection;
        }

        return null; // No collision
    }
}

class Shape {
    constructor(points) {
        this.points = points;

        this.length = 0;
        points.forEach((point, idx) => {
            var nextPoint = points[idx+1];
            if (nextPoint) this.length += point.distance(nextPoint);
            else this.length += point.distance(points[0]);
        });
        console.log('Shape Constructor Length:', this.length);
    }

    draw() {
        this.points.forEach((point, idx) => {
            var idx2 = (idx+1)%this.points.length;
            drawLine(point, this.points[idx2]);
            drawDot(point, 3);
        });
    }
}

class FormationSketch extends Shape {
    constructor(shape, sample) {
        super(shape.points);
        this.sample = sample || 30;
        this._resampleBoundaryPoints();
        this._generateFormationRadiuses();
        this._generateFormationFeatures();
        this._calcSample();
    }

    resample() {
        console.log('resample()');
        //return this;
        return new FormationSketch(this, this.optimizedSample);
    }

    draw() {
        super.draw();

        /*this.svd.V.forEach(function (v) {
            drawLine(center, new Vector2(center.x + v[0]*100, center.y + v[1]*100));
        });*/

        this.positions.forEach((point, idx) => {
            drawDot(point, 3);
            var features = this.positionFeatures[idx];
            
            ctx.font = "12px serif";
            //ctx.fillText(Math.floor(features[0]), point.x, point.y + 10);
            ctx.fillText(idx, point.x, point.y + 10);
        });
        drawDot(this.center, 13);
    }

    _calcSample() {
        //this.sample = 70;
        //console.log(numBoundaryPoints);
        //console.log(numInsidePoints);
        console.log('Sample:', this.sample);
        //var n = this.numBoundaryPoints + this.numInsidePoints;
        //n = n/2;
        //var n = 50;
        //var ba = Math.min(this.numBoundaryPoints, numAgents);
        //var ba = Math.min(this.numBoundaryPoints, this.numBoundaryPoints);
        //var ia = Math.min(n-ba, this.numInsidePoints);
        //var ia = Math.min(this.numInsidePoints, this.numInsidePoints);
        //var r = ba/ia;
        //var r = 0.33;

        //console.log(r);
        console.log('BP:', this.numBoundaryPoints);
        console.log('IP:', this.numInsidePoints);
        //console.log('');
        //console.log('BA:', ba);
        //console.log('IA:', ia);
        //console.log('R:', r);
        console.log('L:', this.length);

        var testNumAgents = this.numBoundaryPoints + this.numInsidePoints;
        testNumAgents = testNumAgents / 2;
        //var n = testNumAgents;
        //n = Math.floor(n);
        //n = 100;
        var p = 0.9;


        var bp = this.numBoundaryPoints;
        var fp = this.numInsidePoints;

        var pf = this.length;
        //var pf = 20;

        /*var a = 1;
        var b = 2 * (n + (Math.sqrt(fp)/bp));
        var c = n * n;

        var s = solveQuadratic(a, b, c);

        var su = (pf*p)/s.inte;
        console.log('SU:', su);*/



        //[30, 60, 90, 120, 150].forEach((n, idx) => {

        var n = 50;

            console.log('----------------------------');
            console.log('N:', n);
            //var r = this.numBoundaryPoints/this.numInsidePoints;
            var r = (this.numBoundaryPoints+this.numInsidePoints)/this.numInsidePoints;
            //var r = 0.5;
            var a = 1;
            var b = r*r;
            var c = -(r*r) * n;

            var s = solveQuadratic(a, b, c);

            let fb = Math.max(s.fba1, s.fba2);
            console.log('B:', fb);
            var su = pf/fb;
            //var su = pf / ((Math.abs(s.fba1)+Math.abs(s.fba2))/2)
            console.log('SU:', su);


        this.optimizedSample = Math.abs(Math.round(su));


        //});


        
        return;









        var r = this.numBoundaryPoints/this.numInsidePoints;
        //var n = 100;
        var n = this.numBoundaryPoints + this.numInsidePoints;
        console.log('Num Agents:', n);
        var a = 1;
        var b = r * r;
        var c = (-b) * n;

        //console.log(a, b, c);

        //var n = numAgents;
        //var a = 1;
        //var b = 2 * (n + (Math.sqrt(this.numInsidePoints)/this.numBoundaryPoints));
        //var c = n * n;

        console.log(a, b, c);

        var fba = (-b + Math.sqrt((b*b) - (4 * a * c))) / (2 * a);
        fba = Math.abs(fba);
        console.log('B:', fba);
        console.log('L:', this.length);
        var newSample = (this.length*0.9)/fba;


        //this.sample = Math.abs(newSample);
        console.log('OS:', newSample);
        this.optimizedSample = newSample;
        //this.sample = 50;
    }

    _resampleBoundaryPoints(resampled) {
        var points = this.points;
        points.push(points[0]);

        var lastPoint = points.shift();
        var resampledPoints = [lastPoint];

        points.forEach((point) => {
            var vector = point.clone().subtract(lastPoint);
            var length = vector.length();

            while (length >= this.sample) {
                var newPoint = vector.clone().length(this.sample).add(lastPoint);
                lastPoint = newPoint;
                resampledPoints.push(newPoint);
                length -= this.sample;
            }
        });

        this.center = (() => {
            var total = new Vector2();
            resampledPoints.forEach(position => {
                total.addX(position.x);
                total.addY(position.y);
            });

            total.div(resampledPoints.length);
            total.round();
            return total;
        })();

        this.points = resampledPoints;

        this.numBoundaryPoints = this.points.length;
        this.numInsidePoints = this._floodFillPositions();

        /*if (!resampled) {
            //console.log(numBoundaryPoints);
            //console.log(numInsidePoints);
            var n = numAgents;
            var ba = Math.min(numBoundaryPoints, numAgents);
            var ia = Math.min(n-ba, numInsidePoints);
            var r = ba/ia;
            r = 1;
            //var r = numBoundaryPoints/numInsidePoints;

            console.log(r);
            console.log('Num Agents:', n);
            console.log('');
            console.log('BA:', ba);
            console.log('IA:', ia);
            console.log('R:', r);

            var rs = r * r;
            var rsn = rs * n;

            //var newSample = (rs + Math.sqrt((rs*rs) - 4 * rsn))/2;
            //console.log(rs);
            //console.log(rsn);
            var b = (-rs + Math.sqrt(Math.abs((rs*rs) - 4 * rsn)))/2;
            console.log('B:', b);
            var newSample = totalLength/b;
            console.log('newSample:', newSample);
            //newSample = 30;
            this._resampleBoundaryPoints(newSample);
        }*/
    }

    _checked(point) {
        var checked = false;
        this.checked.some(checkedPoint => {
            if (point.isEqual(checkedPoint)) checked = true
            return checked;
        });
        return checked;
    };

    _floodFillPositions() {
        console.log('floodFill');

        var fp = [];
        this.checked = [];

        //var sample = this.sample;
        var sample = 30;
        var start = this.center.clone().addX(sample);
        var queue = [start];
        var depth = 20;

        // debugging
        window.fp = this.fp;
        window.checked = this.checked;

        while (queue.length) {
            let point = queue.shift();
            let pleft = point.clone();
            let pright = point.clone().addX(sample);

            [pleft, pright].forEach((p, idx) => {
                while (!this._checked(p) && GeoUtils.pointInBoundDepth(this.points, p, depth)) {
                    fp.push(p.clone());
                    this.checked.push(p.clone());

                    let ptop = p.clone().addY(-sample);
                    if (!this._checked(ptop) && GeoUtils.pointInBoundDepth(this.points, ptop, depth)) queue.push(ptop);
                        
                    let pbottom = p.clone().addY(sample);
                    if (!this._checked(pbottom) && GeoUtils.pointInBoundDepth(this.points, pbottom, depth)) queue.push(pbottom);
                    p = p.clone();
                    idx ? p.addX(sample) : p.addX(-sample);
                }
            });
        }

        let clonedPoints = this.points.map(point => point.clone());
        this.positions = [...fp, ...clonedPoints];
        return fp.length;
    }

    _generateFormationRadiuses() {
        return;
        this.radiuses = [];

        let arrayPositions = this.positions.map(position => position.toArray());
        this.svd = numeric.svd(arrayPositions);
        var vector = this.svd.V[0];
        vector = new Vector2(vector[0], vector[1]);

        /*for (var i = 0; i < 8; ++i) {
            //vector.log();
            if (i == 0) ctx.strokeStyle = "blue";
            else ctx.strokeStyle = "black";
            drawLine(this.center, this.center.clone().add(vector.clone().length(200)));
            this.points.forEach((point, idx) => {
                let point2 = this.points[(idx+1)%this.points.length];
                let intersection = GeoUtils.lineSegmentIntersection(this.center, this.center.clone().add(vector.clone().length(2000)), point, point2);
                if (intersection) {
                    this.radiuses.push([vector.angle(), intersection.distance(this.center)]);
                }
            });

            vector.rotate(45);
        }*/

        //console.log(this.radiuses);
    }

    _generateFormationFeatures() {
        this.positionFeatures = [];
        this.positions.forEach(position => {
            position = position.clone().subtract(this.center);
            this.positionFeatures.push([position.angle(), 0, 0])
        });
    }
}

class ShapeDrawUtils {
    static startShape(cb) {
        var points = [];
        var segmentLength = 20;

        var mouseMove = (e) => {
            var point = new Vector2(e.clientX, e.clientY);
            if (points.slice(-1)[0].distance(point) < segmentLength) return;
            points.push(point);
        }

        var mouseDown = (e) => {
            points.push(new Vector2(e.clientX, e.clientY));
            window.removeEventListener('mousedown', mouseDown);
            window.addEventListener('mouseup', mouseUp);
            window.addEventListener('mousemove', mouseMove);
        }

        var mouseUp = (e) => {
            window.removeEventListener('mouseup', mouseUp);
            window.removeEventListener('mousemove', mouseMove);

            cb(new Shape(points));
        }

        window.addEventListener('mousedown', mouseDown);
    }
}

class Vector2 {
    constructor(x, y) {
        this._v = [x || 0, y || 0];
    }

    get x() {
        return this._v[0];
    }

    get y() {
        return this._v[1];
    }

    clone() {
        return new Vector2(this._v[0], this._v[1]);
    }

    add(vector) {
        this.addX(vector._v[0]);
        this.addY(vector._v[1]);
        return this;
    }

    addX(x) {
        this._v[0] += x;
        return this;
    }

    addY(y) {
        this._v[1] += y;
        return this;
    }

    subtract(vector) {
        this._v[0] -= vector._v[0];
        this._v[1] -= vector._v[1];
        return this;
    }

    mult(scale) {
        this._v[0] *= scale;
        this._v[1] *= scale;
        return this;
    }

    div(scale) {
        this._v[0] /= scale;
        this._v[1] /= scale;
        return this;
    }

    isEqual(vector) {
        return (this._v[0] == vector._v[0] && this._v[1] == vector._v[1]);
    }

    length(scale) {
        if (!scale) return Math.sqrt(Math.pow(this._v[0],2) + Math.pow(this._v[1],2));
        return this.normalize().mult(scale);
    }

    normalize() {
        var l = this.length();
        this._v[0] /= l;
        this._v[1] /= l;
        return this;
    }

    distance(vector) {
        return vector.clone().subtract(this).length();
    }

    angle() {
        return Math.atan2(this._v[0], this._v[1])*(180/Math.PI);
    }

    round() {
        this._v[0] = Math.round(this._v[0]);
        this._v[1] = Math.round(this._v[1]);
        return this;
    }

    rotate(degrees, round) {
        round = round || 1;

        var theta = (Math.PI/180) * degrees;
        var cs = Math.cos(theta);
        var sn = Math.sin(theta);

        var px = this._v[0] * cs - this._v[1] * sn;
        var py = this._v[0] * sn + this._v[1] * cs;

        this._v[0] = px;
        this._v[1] = py;

        return this;
    }

    toArray() {
        return this._v.slice();
    }

    log() {
        console.log(this._v[0], this._v[1]);
    }
}

ShapeDrawUtils.startShape(shape => {
    formationSketch = new FormationSketch(shape).resample();
    //formationSketch = new FormationSketch(shape);
    formationSketch.draw();
});
