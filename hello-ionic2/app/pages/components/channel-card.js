import {Component} from 'angular2/core';
import {StreamService} from '../../services.js';

@Component({
    selector: 'channel-card',
    templateUrl: 'build/pages/components/channel-card.html',
    properties: ['channel: channel'],
    providers: [StreamService],
})
export class ChannelCard {
    static get parameters() {
        return [[StreamService]];
    }

    constructor(streamService) {
        console.log(streamService);
        this.streamService = streamService;
        console.log("Has card?: " + !!this.channel);
    }

    setChannel() {
        this.streamService.setChannel(this.channel);
    }
}
