import {Page} from 'ionic-angular';
import {StreamService} from '../../services.js';
import {Page1} from '../page1/page1';
import {Page2} from '../page2/page2';
import {Page3} from '../page3/page3';
import {CORE_DIRECTIVES} from 'angular2/common';

@Page({
    templateUrl: 'build/pages/tabs/tabs.html',
    directives: [CORE_DIRECTIVES],
    providers: [StreamService],
})
export class TabsPage {
    static get parameters() {
        return [[StreamService]];
    }

    constructor(streamService: StreamService) {
        this.streamService = streamService;

        this.tab1Root = Page1;
        this.tab2Root = Page2;
        this.tab3Root = Page3;
    }
}
