import {NgZone} from 'angular2/core';
import {Page} from 'ionic-angular';
import {TwitchService} from '../../services.js';

@Page({
    templateUrl: 'build/pages/page1/page1.html',
    providers: [TwitchService],
})
export class Page1 {
    static get parameters() {
        return [[TwitchService], [NgZone]];
    }

    constructor(twitchService, zone) {
        this.twitch = twitchService;
    }
}
