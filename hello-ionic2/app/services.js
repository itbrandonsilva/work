import {Injectable} from 'angular2/core';
import {Http} from 'angular2/http';

@Injectable()
export class TwitchService {
    static get parameters() {
        return [];
    }

    constructor() {
        this.channels = [];
        Twitch.init({clientId: 'null'}, (err, status) => {
            if (err) throw err;
            this.reloadStreams();
        });
    }

    reloadStreams() {
        Twitch.api({
            method: 'streams',
            params: {limit: 40} 
        }, (err, res) => {
            if (err) return console.error(err);
            this.channels.length = 0;
            this.channels = this.channels.concat(res.streams);
            console.log(this.channels);
        }); 
    };
}

@Injectable()
export class StreamService {
    static get parameters() {
        return [[Http]];
    }

    constructor(http) {
        this.host = null;
        this.http = http;
        setTimeout(() => {
            this.host = true;
        }, 6000);
    }

    setChannel(channel) {
        this.http.put("http://192.168.1.20:3000/stream/play/" + channel.channel.name).subscribe(res => {});
    }
}
